#include <algorithm>
#include <iostream>
#include <vector>

enum TVerticesColorType {
    VCT_WHITE,
    VCT_GREY,
    VCT_BLACK
};

class Graph {
public:
    explicit Graph( const std::vector<std::vector<int>>& m ) {
        matrix = m;
        size = m.size();
    }
    explicit Graph( int size ) {
        this->size = size;
        matrix = std::vector<std::vector<int>>( size );
        for( auto& i: matrix ) {
            i.resize( size );
        }
    }

    void AddEdge( int k, int m ) {
        matrix[k][m] = matrix[m][k] = 1;
    }
    bool ContainsEdge( int k, int m ) const {
        return ( matrix[k][m] == 1 );
    }

    bool Planar() const {
        // Ищем цикл, если его нет, до граф не соответствует условиям алгоритма
        // (Нет циклов => дерево => планарный)
        getCycle();
        if( cycle.empty() ) {
            return true;
        }
        // Списки граней
        extFace = cycle;
        intFaces.resize( 0 );
        intFaces.push_back( cycle );
        intFaces.push_back( extFace );

        // Массивы уже уложенных вершин и ребер соответственно
        laidVertices.resize( size, 0 );
        laidEdges.resize( size, std::vector<bool>( size ) );
        for( int i : cycle ) {
            laidVertices[i] = true;
        }

        // Укладываем найденный цикл
        layChain( cycle, true );

        // Второй шаг алгоритма:
        // выделение множества сегментов, подсчет числа вмещающих граней,
        // выделение цепей из сегментов, укладка цепей, добавление новых граней
        while( true ) {
            segments.clear();
            getSegments();

            // Если нет сегментов, то уже уложили
            if( segments.empty() ) {
                break;
            }

            // Массив граней, в которые будут уложены соответствующие сегменты с минимальным числом calcNumOfFacesContainedSegments()
            std::vector<int> countFaces( segments.size() );
            destFaces.resize( segments.size(), std::vector<int>() );
            calcNumOfFacesContainedSegments( countFaces );

            // Ищем минимальное число calcNumOfFacesContainedSegments()
            int min = 0;
            for( int i = 0; i < segments.size(); i++ ) {
                if( countFaces[i] < countFaces[min] ) {
                    min = i;
                }
            }

            // Если хотя для одного сегмента нет подходящих граней, то граф не планарный
            if( countFaces[min] == 0 ) {
                return false;
            }
            segmentLaying( min );
        }
        return true;
    }

private:
    std::vector<std::vector<int>> matrix;
    int size = 0;

    mutable std::vector<int> cycle;
    mutable std::vector<bool> laidVertices;
    mutable std::vector<std::vector<bool>> laidEdges;
    mutable std::vector<Graph> segments;
    mutable std::vector<std::vector<int>> destFaces;
    mutable std::vector<std::vector<int>> intFaces;
    mutable std::vector<int> extFace;
    mutable std::vector<int> face;
    mutable std::vector<int> face1;
    mutable std::vector<int> face2;

    // Поиск простого цикла, используя DFS
    bool dfsCycle( std::vector<int>& result, std::vector<int>& used, int parent, int v ) const {
        used[v] = VCT_GREY;
        for( int i = 0; i < size; i++ ) {
            if( i == parent ) {
                continue;
            }
            if( matrix[v][i] == 0 ) {
                continue;
            }
            if( used[i] == VCT_WHITE ) {
                result.push_back( v );
                if( dfsCycle( result, used, v, i ) ) {
                    //Цикл найден
                    return true;
                } else {
                    result.pop_back();
                }
            }
            if( used[i] == VCT_GREY ) {
                result.push_back( v );
                //Найден цикл
                std::vector<int> cycle;
                //Выдергиваем вершины цикла из порядка обхода
                for( int j = 0; j < result.size(); j++ ) {
                    if( result[j] == i ) {
                        cycle.insert( cycle.end(), result.begin() + j, result.end() );
                        result.clear();
                        result.insert( result.end(), cycle.begin(), cycle.end() );
                        return true;
                    }
                }
                return true;
            }
        }
        used[v] = VCT_BLACK;
        return false;
    }

    // Поиск связных компонент графа G\G_plane, дополненного ребрами из G,
    // один из концов которых принадлежит связной компоненте, а другой G_plane
    void dfsSegments( std::vector<int>& used, Graph& result, int v ) const {
        used[v] = VCT_GREY;
        for( int i = 0; i < size; i++ ) {
            if( matrix[v][i] == 1 ) {
                result.AddEdge( v, i );
                if( used[i] == VCT_WHITE && !laidVertices[i] ) {
                    dfsSegments( used, result, i );
                }
            }
        }
    }

    void getSegments() const {
        // Поиск однореберных сегментов
        for( int i = 0; i < size; i++ ) {
            for( int j = i + 1; j < size; j++ ) {
                if( matrix[i][j] == 1 && !laidEdges[i][j] && laidVertices[i] && laidVertices[j] ) {
                    Graph t( size );
                    t.AddEdge( i, j );
                    segments.push_back( t );
                }
            }
        }
        // Поиск связных компонент графа G\G_plane, дополненного ребрами из G,
        // один из концов которых принадлежит связной компоненте, а другой G_plane
        std::vector<int> used( size );
        for( int i = 0; i < size; i++ ) {
            if( used[i] == VCT_WHITE && !laidVertices[i] ) {
                Graph res( size );
                dfsSegments( used, res, i );
                segments.push_back( res );
            }
        }
    }

    // Поиск цепи в выбранном сегменте, используя DFS
    void dfsChain( std::vector<int>& used, std::vector<int>& chain, int v ) const {
        used[v] = VCT_GREY;
        chain.push_back( v );
        for( int i = 0; i < size; i++ ) {
            if( matrix[v][i] == 1 && used[i] == VCT_WHITE ) {
                if( !laidVertices[i] ) {
                    dfsChain( used, chain, i );
                } else {
                    chain.push_back( i );
                }
                return;
            }
        }
    }

    // Поиск цепи  ежду контактными вершинами в сегменте
    void getChain( std::vector<int>& result ) const {
        for( int i = 0; i < size; i++ ) {
            if( laidVertices[i] ) {
                bool inGraph = false;
                for( int j = 0; j < size; j++ ) {
                    if( ContainsEdge( i, j ) ) {
                        inGraph = true;
                    }
                }
                if( inGraph ) {
                    std::vector<int> used( size );
                    dfsChain( used, result, i );
                    break;
                }
            }
        }
    }

    // Проверка на то, что данный сегмент содержится в данной грани
    bool isFaceContainsSegment( const std::vector<int>& face, const Graph& segment ) const {
        for( int i = 0; i < size; i++ ) {
            for( int j = 0; j < size; j++ ) {
                if( segment.ContainsEdge( i, j ) ) {
                    if( ( laidVertices[i] && ( std::find( face.begin(), face.end(), i ) == face.end() ) )
                       || ( laidVertices[j] && ( std::find( face.begin(), face.end(), j ) == face.end() ) ) ) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    // Считаем число граней, вмещающих данные сегмент
    void calcNumOfFacesContainedSegments( std::vector<int>& countFaces ) const {
        for( int i = 0; i < segments.size(); i++ ) {
            for( auto& face: intFaces ) {
                if( isFaceContainsSegment( face, segments[i] ) ) {
                    destFaces[i] = face;
                    countFaces[i]++;
                }
            }
            if ( isFaceContainsSegment( extFace, segments[i] ) ) {
                destFaces[i] = extFace;
                countFaces[i]++;
            }
        }
    }

    bool getCycle() const {
        std::vector<int> used( size );
        bool hasCycle = dfsCycle( cycle, used, -1, 0 );
        if( !hasCycle ) {
            cycle = std::vector<int>();
        }
        return !cycle.empty();
    }

    // Укладка цепи, описание матрицы смежности
    void layChain( std::vector<int>& chain, bool cyclic ) const {
        for( int i = 0; i < chain.size() - 1; i++ ) {
            laidEdges[chain[i]][chain[i + 1]] = true;
            laidEdges[chain[i + 1]][chain[i]] = true;
        }
        if( cyclic ) {
            laidEdges[chain[0]][chain[chain.size() - 1]] = true;
            laidEdges[chain[chain.size() - 1]][chain[0]] = true;
        }
    }

    void chainLayingOnFace( int contactFirst, int contactSecond, const std::vector<int>& chain ) const {
        // Находим обратную цепь(цепь, пробегаемая в обратном направлении)
        std::vector<int> reverseChain = chain;
        for( int i = 0; i <= reverseChain.size() / 2; ++i ){
            std::swap( reverseChain[i], reverseChain[reverseChain.size() - 1 - i] );
        }
        int faceSize = face.size();
        if( face != extFace ) {
            layingInIntFace( contactFirst, contactSecond, chain, reverseChain, faceSize );
        } else {
            layingInExtFace( contactFirst, contactSecond, chain, reverseChain, faceSize );
        }
    }

    void segmentLaying( int min ) const {
        // Укладка выбранного сегмента
        // Выделяем цепь между двумя контактными вершинами
        std::vector<int> chain;
        segments[min].getChain( chain );

        // Помечаем вершины цепи как уложенные
        for( int i: chain ) {
            laidVertices[i] = true;
        }

        // Укладываем соответствующие ребра цепи
        layChain( chain, false );

        // Целевая грань, куда будет уложен выбранный сегмент
        face = destFaces[min];

        // Новые грани, порожденные разбиением грани face выбранным сегментом
        face1.resize( 0 );
        face2.resize( 0 );

        // Ищем номера контактных вершин цепи
        int contactFirst = 0, contactSecond = 0;
        for( int i = 0; i < face.size(); i++ ) {
            if( face[i] == chain[0] ) {
                contactFirst = i;
            }
            if( face[i] == chain[chain.size() - 1] ) {
                contactSecond = i;
            }
        }
        chainLayingOnFace( contactFirst, contactSecond, chain );
    }

    void layingInExtFace( int contactFirst, int contactSecond, const std::vector<int>& chain, const std::vector<int>& reverseChain,
                                int faceSize ) const {
        // Если целевая грань совпала с внешней
        // Все то же самое, только одна из порожденных граней - новая внешняя грань
        std::vector<int> newOuterFace;
        if( contactFirst < contactSecond ) {
            newOuterFace.insert( newOuterFace.end(), chain.begin(), chain.end() );
            for( int i = ( contactSecond + 1 ) % faceSize; i != contactFirst; i = ( i + 1 ) % faceSize ) {
                newOuterFace.push_back( face[i] );
            }
            face2.insert( face2.end(), chain.begin(), chain.end() );
            for( int i = ( contactSecond - 1 + faceSize ) % faceSize; i != contactFirst; i = ( i - 1 + faceSize ) % faceSize ) {
                face2.push_back( face[i] );
            }
        } else {
            newOuterFace.insert( newOuterFace.end(), reverseChain.begin(), reverseChain.end() );
            for( int i = ( contactFirst + 1 ) % faceSize; i != contactSecond; i = ( i + 1 ) % faceSize ) {
                newOuterFace.push_back( face[i] );
            }
            face2.insert( face2.end(), reverseChain.begin(), reverseChain.end() );
            for( int i = ( contactFirst - 1 + faceSize ) % faceSize; i != contactSecond; i = ( i - 1 + faceSize ) % faceSize ) {
                face2.push_back( face[i] );
            }
        }
        // Удаляем старые, добавляем новые
        intFaces.erase( std::find( intFaces.begin(), intFaces.end(), extFace ) );
        intFaces.push_back( newOuterFace );
        intFaces.push_back( face2 );
        extFace = newOuterFace;
    }

    void layingInIntFace( int contactFirst, int contactSecond, const std::vector<int>& chain, const std::vector<int>& reverseChain,
                                int faceSize ) const {
        // Если целевая грань не внешняя
        // Укладываем прямую цепь в одну из порожденных граней,
        // а обратную в другую в зависимости от номеров контактных вершин
        if( contactFirst < contactSecond ) {
            face1.insert( face1.end(), chain.begin(), chain.end() );
            for( int i = ( contactSecond + 1 ) % faceSize; i != contactFirst; i = ( i + 1 ) % faceSize ) {
                face1.push_back( face[i] );
            }
            face2.insert( face2.end(), reverseChain.begin(), reverseChain.end() );
            for( int i = ( contactFirst + 1 ) % faceSize; i != contactSecond; i = ( i + 1 ) % faceSize ) {
                face2.push_back( face[i] );
            }
        } else {
            face1.insert( face1.end(), reverseChain.begin(), reverseChain.end() );
            for( int i = ( contactFirst + 1 ) % faceSize; i != contactSecond; i = ( i + 1 ) % faceSize ) {
                face1.push_back( face[i] );
            }
            face2.insert( face2.end(), chain.begin(), chain.end() );
            for( int i = ( contactSecond + 1 ) % faceSize; i != contactFirst; i = ( i + 1 ) % faceSize ) {
                face2.push_back( face[i] );
            }
        }
        // Удаляем целевую грань(она разбилась на две новые)
        // Добавляем порожденные грани в множество внутренних граней
        intFaces.erase( std::find( intFaces.begin(), intFaces.end(), face ) );
        intFaces.push_back( face1 );
        intFaces.push_back( face2 );
    }
};

int main() {
    int size = 0;
    std::cin >> size;
    int numEdges = 0;
    Graph graph( size );
    std::cin >> numEdges;
    for( int i = 0; i < numEdges; ++i ) {
        int u ,v;
        std::cin >> u >> v;
        graph.AddEdge( u, v );
    }
    std::cout << ( graph.Planar() ? "YES" : "NO" ) << std::endl;
}
