#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "FindShortestWay.h"

TEST_CASE("test example 1") {
    INFO("TEST_CASE:test example 1 started!");
    int a = 3;
    int b = 14;
    int numOfVertices = 15;
    int x = 9;
    int y = 9;
    REQUIRE(0 == FindShortestWay(numOfVertices, a, b, x, y));
}

TEST_CASE("test example 2") {
    INFO("TEST_CASE:test example 2 started!");
    int a = 6;
    int b = 1;
    int numOfVertices = 5;
    int x = 2;
    int y = 3;
    REQUIRE(6 == FindShortestWay(numOfVertices, a, b, x, y));
}

TEST_CASE("test example 3") {
    INFO("TEST_CASE:test example 3 started!");
    int a = 6;
    int b = 1;
    int numOfVertices = 5;
    int x = 2;
    int y = 1;
    REQUIRE(2 == FindShortestWay(numOfVertices, a, b, x, y));
}