#include "FindShortestWay.h"
#include <vector>
#include <queue>

int FirstRuleEdgeEnd(int vertex, int numOfVertices) {
    return (vertex + 1) % numOfVertices;
}

int SecondRuleEdgeEnd(int vertex, int numOfVertices) {
    return (vertex * vertex + 1) % numOfVertices;
}

int NextQueueNumber(int currentNumber, int k, int step = 1) {
    return (currentNumber + step) % (k + 1);
}

// Используется алгоритм k очередей
int FindShortestWay(int numOfVertices, int firstRuleEdgeLength, int secondRuleEdgeLength, int start, int end) {
    int k = std::max(firstRuleEdgeLength, secondRuleEdgeLength);
    std::vector<std::queue<int>> queues(k + 1, std::queue<int>());
    long long currentVertex = start;
    std::vector<int> distance(numOfVertices, -1);
    distance[start] = 0;
    queues[0].push(start);
    int currentQueue = 0;
    while (currentVertex != end) {
        while (!queues[currentQueue].empty()) {
            currentVertex = queues[currentQueue].front();
            queues[currentQueue].pop();
            int firstVertex = FirstRuleEdgeEnd(currentVertex, numOfVertices);
            int secondVertex = SecondRuleEdgeEnd(currentVertex, numOfVertices);
            if (firstVertex == secondVertex) {
                if (distance[firstVertex] == -1) {
                    int minEdge = std::min(firstRuleEdgeLength, secondRuleEdgeLength);
                    distance[firstVertex] = distance[currentVertex] + minEdge;
                    queues[NextQueueNumber(currentQueue, k, minEdge)].push(firstVertex);
                }
            } else {
                if (distance[firstVertex] == -1) {
                    distance[firstVertex] = distance[currentVertex] + firstRuleEdgeLength;
                    queues[NextQueueNumber(currentQueue, k, firstRuleEdgeLength)].push(firstVertex);
                }
                if (distance[secondVertex] == -1) {
                    distance[secondVertex] = distance[currentVertex] + secondRuleEdgeLength;
                    queues[NextQueueNumber(currentQueue, k, secondRuleEdgeLength)].push((secondVertex);
                }
            }
            if (distance[end] != -1) {
                return distance[end];
            }
        }
        currentQueue = NextQueueNumber(currentQueue, k);
    }
    return distance[end];
}