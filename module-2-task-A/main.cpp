#include "FindShortestWay.h"
#include <iostream>
#include <vector>
#include <queue>

int main() {
    int numOfVertices;
    int a;
    int b;
    int x;
    int y;
    std::cin >> a >> b >> numOfVertices >> x >> y;
    std::cout << FindShortestWay(numOfVertices, a, b, x, y);
}
