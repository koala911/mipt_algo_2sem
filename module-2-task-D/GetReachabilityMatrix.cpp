#include "GetReachabilityMatrix.h"

void SetBit(uint16_t& n) {
    n |= 1;
}

bool GetBit(uint16_t n) {
    return (n &= 1);
}

void GetReachabilityMatrix(int numOfVertices, std::vector<std::vector<uint16_t>>& reachabilityMatrix) {
    for (int k = 0; k < numOfVertices; ++k) {
        for (int i = 0; i < numOfVertices; ++i) {
            if (GetBit(reachabilityMatrix[i][k])) {
                for (int j = 0; j < numOfVertices + 1; ++j) {
                    reachabilityMatrix[i][j] |= reachabilityMatrix[k][j];
                }
            }
        }
    }
}
