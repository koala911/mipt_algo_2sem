#include <cstdint>
#include <iostream>
#include <vector>
#include "GetReachabilityMatrix.h"

int main() {
    int numOfVertices = 0;
    std::cin >> numOfVertices;
    std::vector<std::vector<uint16_t >> adjacencyMatrix(numOfVertices, std::vector<uint16_t>(numOfVertices + 1, 0));
    for (int i = 0; i < numOfVertices; ++i) {
        for (int j = 0; j < numOfVertices; ++j) {
            char current;
            std::cin >> current;
            if (current - '0') {
                SetBit(adjacencyMatrix[i][j]);
            }
        }
    }
    GetReachabilityMatrix(numOfVertices, adjacencyMatrix);
    for (int i = 0; i < numOfVertices; ++i) {
        for (int j = 0; j < numOfVertices; ++j) {
            std::cout << GetBit(adjacencyMatrix[i][j]);
        }
        std::cout << '\n';
    }
}
