#pragma once

#include <vector>

void GetReachabilityMatrix(int numOfVertices, std::vector<std::vector<uint16_t>>& reachabilityMatrix);

void SetBit(uint16_t& n);

bool GetBit(uint16_t n);