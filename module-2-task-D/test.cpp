#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "GetReachabilityMatrix.h"
#include <vector>

TEST_CASE("Test from task") {
    INFO("TEST_CASE:Test from task started!");
    int numOfVertices = 3;
    std::vector<std::vector<uint16_t >> adjacencyMatrix = {
            {0, 1, 0},
            {0, 0, 1},
            {0, 0, 0}
    };
    GetReachabilityMatrix(numOfVertices, adjacencyMatrix);
    std::vector<std::vector<uint16_t >> correctAnswer = {
            {0, 1, 1},
            {0, 0, 1},
            {0, 0, 0}
    };
    REQUIRE(adjacencyMatrix == correctAnswer);
}

TEST_CASE("Test zero matrix") {
    INFO("TEST_CASE:Test zero matrix started!");
    int numOfVertices = 3;
    std::vector<std::vector<uint16_t >> adjacencyMatrix = {
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0}
    };
    GetReachabilityMatrix(numOfVertices, adjacencyMatrix);
    std::vector<std::vector<uint16_t >> correctAnswer = {
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0}
    };
    REQUIRE(adjacencyMatrix == correctAnswer);
}

TEST_CASE("Test unit matrix") {
    INFO("TEST_CASE:Test unit matrix started!");
    int numOfVertices = 3;
    std::vector<std::vector<uint16_t >> adjacencyMatrix = {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
    };
    GetReachabilityMatrix(numOfVertices, adjacencyMatrix);
    std::vector<std::vector<uint16_t >> correctAnswer = {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
    };
    REQUIRE(adjacencyMatrix == correctAnswer);
}
