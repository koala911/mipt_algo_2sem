#include <iostream>
#include "SparseTable.h"


int main() {
    size_t n;
    size_t m;
    std::cin >> n >> m;
    std::vector<int> sequence(n);
    for (size_t i = 0; i < n; i++) {
        int x = 0;
        std::cin >> x;
        sequence[i] = x;
    }
    SparseTable sparseTable(sequence);
    for (size_t i = 0; i < m; i++) {
        size_t left = 0;
        size_t right = 0;
        std::cin >> left >> right;
        std::cout << sparseTable.GetSecond(left - 1, right - 1) << '\n';
    }
}