#include "SparseTable.h"


SparseTable::SparseTable(const std::vector<int>& sequence): sequence(sequence), size(sequence.size()), sparseTable(ceil(std::log2(size)), std::vector<std::pair<int, int>>(size, std::pair<int, int>(0, 0))) {
    for (size_t i = 0; i < size + 1; i++) {
        log2.push_back(ceil(std::log2(i)));
    }
    for (size_t i = 0; i < size; i++) {
        sparseTable[0][i].first = this->sequence[i];
        sparseTable[0][i].second = this->sequence[i];
    }
    for (size_t i = 1; i < sparseTable.size(); i++) {
        for (size_t j = 0; j < size - (1 << i) + 1; j++) {
            std::pair<int, int> minPair = getMinPair(sparseTable[i - 1][j], sparseTable[i - 1][j + (1 << (i - 1))]);
            sparseTable[i][j] = minPair;
        }
    }
}

int SparseTable::GetSecond(size_t a, size_t b) {
    size_t k = log2[b - a + 1] - 1;
    return getMinPair(sparseTable[k][a], sparseTable[k][b - (1 << k) + 1]).second;
}

std::pair<int, int> SparseTable::getMinPair(std::pair<int, int> left, std::pair<int, int> right) {
    std::pair<int, int> result;
    std::vector<int> order{left.first, left.second, right.first, right.second};
    std::sort(order.begin(), order.end());
    result.first = order[0];
    for (int i = 1; i < 4; i++)
        if (order[i] != order[0]) {
            result.second = order[i];
            break;
        }
    return result;
}