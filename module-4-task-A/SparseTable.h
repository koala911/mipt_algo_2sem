#include <algorithm>
#include <cmath>
#include <vector>


class SparseTable {
public:
    explicit SparseTable(const std::vector<int>& sequence);

    int GetSecond(size_t a, size_t b);

private:
    std::vector<int> sequence;
    size_t size;
    std::vector<int> log2;
    std::vector<std::vector<std::pair<int, int>>> sparseTable;

    std::pair<int, int> getMinPair(std::pair<int, int> left, std::pair<int, int> right);
};