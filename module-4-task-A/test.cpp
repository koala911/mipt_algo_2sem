#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "MinCut.h"

TEST_CASE("Test example") {
    INFO("TEST_CASE:Test example started!");
    std::vector<int> sequence = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    SparseTable sparseTable(sequence);
    REQUIRE(sparseTable.GetSecond(0, 1), 2);
    REQUIRE(sparseTable.GetSecond(0, 9), 2);
    REQUIRE(sparseTable.GetSecond(1, 6), 2);
}

TEST_CASE("Test two") {
    INFO("TEST_CASE:Test two started!");
    std::vector<int> sequence = {1, 1, 1, 1, 1, 1, 1};
    SparseTable sparseTable(sequence);
    REQUIRE(sparseTable.GetSecond(0, 1), 1);
    REQUIRE(sparseTable.GetSecond(0, 4), 1);
    REQUIRE(sparseTable.GetSecond(1, 6), 1);
}

TEST_CASE("Test three") {
    INFO("TEST_CASE:Test three started!");
    std::vector<int> sequence = {7, 6, 5, 4, 3, 2, 1};
    SparseTable sparseTable(sequence);
    REQUIRE(sparseTable.GetSecond(0, 1), 6);
    REQUIRE(sparseTable.GetSecond(0, 4), 4);
    REQUIRE(sparseTable.GetSecond(1, 6), 2);
}