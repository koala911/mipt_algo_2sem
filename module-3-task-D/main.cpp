#include <iostream>
#include <vector>
#include "FindMinCost.h"

int main() {
    int n;
    int m;
    int a;
    int b;
    std::cin >> n >> m >> a >> b;
    std::vector<std::vector<bool>> bridge(n, std::vector<bool>(m));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            char cell;
            std::cin >> cell;
            if (cell == '.') {
                bridge[i][j] = true;
            } else if (cell == '*') {
                bridge[i][j] = false;
            }
        }
    }
    std::cout << FindMinCost(n, m, a, b, bridge);
}