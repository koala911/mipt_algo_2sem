#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "FindMinCost.h"

TEST_CASE("Test example") {
    INFO("TEST_CASE:Test example started!");
    int n = 2;
    int m = 3;
    int a = 3;
    int b = 2;
    std::vector<std::vector<bool>> bridge = {
            {true, false, false},
            {true, false, true},
    };
    REQUIRE(FindMinCost(n, m, a, b, bridge), 5);
}

TEST_CASE("Test trivial case") {
    INFO("TEST_CASE:Test trivial case started!");
    int n = 2;
    int m = 3;
    int a = 3;
    int b = 2;
    std::vector<std::vector<bool>> bridge = {
            {true, true, true},
            {true, true, true},
    };
    REQUIRE(FindMinCost(n, m, a, b, bridge), 0);
}

TEST_CASE("Test a >= 2b") {
    INFO("TEST_CASE:Test a >= 2b started!");
    int n = 2;
    int m = 3;
    int a = 3;
    int b = 1;
    std::vector<std::vector<bool>> bridge = {
            {false, false, true},
            {false, true, true},
    };
    REQUIRE(FindMinCost(n, m, a, b, bridge), 3);
}