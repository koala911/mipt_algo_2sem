#include "FindMinCost.h"


bool KuhnAlgorithm(int v, std::vector<int>& currentEdges, std::vector<bool>& used, const std::vector<std::vector<int>>& leftEdges) {
    if (used[v]) {
        return false;
    }
    used[v] = true;
    for (size_t i = 0; i < leftEdges[v].size(); ++i) {
        int to = leftEdges[v][i];
        if (currentEdges[to] == -1 || KuhnAlgorithm(currentEdges[to], currentEdges, used, leftEdges)) {
            currentEdges[to] = v;
            return true;
        }
    }
    return false;
}

void AddEdges(int i, int j, const std::vector<std::vector<int>>& indices, std::vector<std::vector<int>>& edges) {
    try {
        if (indices.at(i).at(j + 1) >= 0) {
            edges[indices.at(i).at(j)].push_back(indices.at(i).at(j + 1));
        }
    } catch (const std::out_of_range& exception) {}
    try {
        if (indices.at(i).at(j - 1) >= 0) {
            edges[indices.at(i).at(j)].push_back(indices.at(i).at(j - 1));
        }
    } catch (const std::out_of_range& exception) {}
    try {
        if (indices.at(i - 1).at(j) >= 0) {
            edges[indices.at(i).at(j)].push_back(indices.at(i - 1).at(j));
        }
    } catch (const std::out_of_range& exception) {}
    try {
        if (indices.at(i + 1).at(j) >= 0) {
            edges[indices.at(i).at(j)].push_back(indices.at(i + 1).at(j));
        }
    } catch (const std::out_of_range& exception) {}
}

int FindMinCost(int n, int m, int a, int b, const std::vector<std::vector<bool>>& bridge) {
    int leftPartSize = 0;
    int rightPartSize = 0;
    std::vector<std::vector<int>> indices(n, std::vector<int>(m, -1));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (!bridge[i][j]) {
                if ((i + j) % 2) {
                    indices[i][j] = leftPartSize;
                    ++leftPartSize;
                } else {
                    indices[i][j] = rightPartSize;
                    ++rightPartSize;
                }
            }
        }
    }
    if (a >= 2 * b) {
        return b * (leftPartSize + rightPartSize);
    }
    std::vector<std::vector<int>> leftEdges(leftPartSize);
    std::vector<std::vector<int>> rightEdges(rightPartSize);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (!bridge[i][j]) {
                if ((i + j) % 2) {
                    AddEdges(i, j, indices, leftEdges);
                } else {
                    AddEdges(i, j, indices, rightEdges);
                }
            }
        }
    }
    std::vector<int> currentEdges(rightPartSize, -1);
    std::vector<bool> used(leftPartSize);
    for (int v = 0; v < leftPartSize; ++v) {
        used.assign(leftPartSize, false);
        KuhnAlgorithm(v, currentEdges, used, leftEdges);
    }
    int edgesCount = 0;
    for (auto vertex: currentEdges) {
        if (vertex != -1) {
            edgesCount += 1;
        }
    }
    return edgesCount * a + (leftPartSize + rightPartSize - 2 * edgesCount) * b;
}