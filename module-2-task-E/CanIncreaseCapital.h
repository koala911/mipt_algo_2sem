#pragma once
#include <vector>

using std::vector;

const double EPS = 1e-6;

struct Edge {
    int from;
    int to;
    double rate;
    double commission;

    Edge(int from, int to, double rate, double commission): from(from), to(to), rate(rate), commission(commission) {}
};

bool CanIncreaseCapital(int numOfVertices, const vector<Edge>& edges, int startVertex, double startValue);