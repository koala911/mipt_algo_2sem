#include "CanIncreaseCapital.h"

bool Relax(long double& oldValue, long double newValue) {
    if (newValue >= oldValue + EPS) {
        oldValue = newValue;
        return true;
    } else {
        return false;
    }
}

// Используется алгоритм Форда-Беллмана
bool CanIncreaseCapital(int numOfVertices, const vector<Edge>& edges, int startVertex, double startValue) {
    vector<long double> maxValue(numOfVertices, 0);
    maxValue[startVertex] = startValue;
    bool wasRelax = false;
    for (int i = 0; i < numOfVertices; ++i) {
        wasRelax = false;
        for (const Edge& edge: edges) {
            int from = edge.from;
            int to = edge.to;
            double rate = edge.rate;
            double commission = edge.commission;
            if (maxValue[from] >= EPS) {
                wasRelax |= Relax(maxValue[to], (maxValue[from] - commission) * rate);
            }
        }
    }
    return wasRelax;
}