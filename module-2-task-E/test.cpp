#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "CanIncreaseCapital.h"
#include <vector>

TEST_CASE("Test from task") {
    INFO("TEST_CASE:Test from task started!");
    int numOfVertices = 3;
    int startVertex = 0;
    double startValue = 10;
    vector<Edge> edges = {
            Edge(1, 0, 1, 1),
            Edge(0, 1, 1, 1),
            Edge(1, 2, 1.1, 1),
            Edge(2, 1, 1.1, 1)
    };
    bool answer = CanIncreaseCapital(numOfVertices, edges, startVertex, startValue);
    REQUIRE(answer == false);
}

TEST_CASE("Test small value") {
    INFO("TEST_CASE:Test small value started!");
    int numOfVertices = 2;
    int startVertex = 0;
    double startValue = 0.1;
    vector<Edge> edges = {
        Edge(1, 0, 1, 1),
        Edge(0, 1, 1, 1),
    };
    bool answer = CanIncreaseCapital(numOfVertices, edges, startVertex, startValue);
    REQUIRE(answer == false);
}

TEST_CASE("Test big value") {
    INFO("TEST_CASE:Test big value started!");
    int numOfVertices = 2;
    int startVertex = 0;
    double startValue = 100;
    vector<Edge> edges = {
        Edge(1, 0, 2, 1),
        Edge(0, 1, 2, 1),
    };
    bool answer = CanIncreaseCapital(numOfVertices, edges, startVertex, startValue);
    REQUIRE(answer == true);
}