#include <iostream>
#include <vector>
#include "CanIncreaseCapital.h"

using std::vector;

int main() {
    int numOfVertices;
    int numOfEdges;
    int startVertex;
    double startValue;
    std::cin >> numOfVertices >> numOfEdges >> startVertex >> startValue;
    vector<Edge> edges;
    for (int i = 0; i < numOfEdges; ++i) {
        int a;
        int b;
        double RAB;
        double CAB;
        double RBA;
        double CBA;
        std::cin >> a >> b >> RAB >> CAB >> RBA >> CBA;
        edges.emplace_back(Edge(a - 1, b - 1, RAB, CAB));
        edges.emplace_back(Edge(b - 1, a - 1, RBA, CBA));
    }
    bool answer = CanIncreaseCapital(numOfVertices, edges, startVertex - 1, startValue);
    std::cout << (answer ? "YES" : "NO");
}
