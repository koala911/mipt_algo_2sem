#include <iostream>
#include <stack>
#include <unordered_set>
#include <vector>

enum TVerticesColorType {
    VCT_WHITE,
    VCT_GREY,
    VCT_BLACK
};

//DFS по инвертированному графу с добавлением вершин в out
void InvertDFS(const std::vector<std::vector<int>>& edges, int startVertex, std::stack<int>& out, std::vector<int>& verticesColor) {
    verticesColor[startVertex] = VCT_GREY;
    for(const auto& v: edges[startVertex]) {
        if(verticesColor[v] == VCT_WHITE) {
            InvertDFS(edges, v, out, verticesColor);
        }
    }
    verticesColor[startVertex] = VCT_BLACK;
    out.push(startVertex);
}

//DFS по графу с добавлением в компоненту сильной связности и подсчетом посещенных вершин
void DFS(const std::vector<std::vector<int>>& edges, int startVertex, std::unordered_set<int>& scc, std::vector<int>& verticesColor, int& verticesCounter) {
    verticesColor[startVertex] = VCT_GREY;
    for(const auto& v: edges[startVertex]) {
        if(verticesColor[v] == VCT_WHITE) {
            DFS(edges, v, scc, verticesColor, verticesCounter);
        }
    }
    verticesColor[startVertex] = VCT_BLACK;
    scc.insert(startVertex);
    ++verticesCounter;
}

//Находит минимальное кол-во ребер, которое нужно добавить для сильной связности
int MinNumOfAddedEdges(int numVertices, const std::vector<std::vector<int>>& edges, const std::vector<std::vector<int>>& invertedEdges) {
    std::stack<int> out;
    std::vector<std::unordered_set<int>> SCC;
    std::vector<int> verticesColor(numVertices);
    for(auto &c: verticesColor) c = VCT_WHITE;
    for(int u = 0; u < numVertices; ++u) {
        if(verticesColor[u] == VCT_WHITE) {
            InvertDFS(invertedEdges, u, out, verticesColor);
        }
    }
    for(auto &c: verticesColor) c = VCT_WHITE;
    std::unordered_set<int> scc;
    int verticesCounter = 0;
    while(verticesCounter < numVertices) {
        int u = out.top();
        out.pop();
        while(out.size() > 0 && verticesColor[u] != VCT_WHITE) {
            u = out.top();
            out.pop();
        }
        scc.clear();
        DFS(edges, u, scc, verticesColor, verticesCounter);
        SCC.push_back(std::move(scc));
    }
    if(SCC.size() == 1) {
        return 0;
    }
    std::vector<int> inDdeg(SCC.size());
    std::vector<int> outDeg(SCC.size());
    for(int i = 0; i < SCC.size(); ++i) {
        for(int u: SCC[i]) {
            for(int v: edges[u]) {
                if(SCC[i].find(v) == SCC[i].end()) ++outDeg[i];
            }
            for(int v: invertedEdges[u]) {
                if(SCC[i].find(v) == SCC[i].end()) ++inDeg[i];
            }
        }
    }
    int nullInDegCounter = 0;
    int nullOutDegCounter = 0;
    for(int in: inDeg) if (in == 0) ++nullInDegCounter;
    for(int out: outDeg) if (out == 0) ++nullOutDegCounter;
    return std::max(nullInDegCounter, nullOutDegCounter);
}

int main() {
    int numVertices = 0;
    int numEdges = 0;
    int u = 0;
    int v = 0;
    std::cin >> numVertices >> numEdges;
    std::vector<std::vector<int>> edges(numVertices), invertedEdges(numVertices);
    for(int i = 0; i < numEdges; ++i) {
        std::cin >> u >> v;
        edges[u - 1].push_back(v - 1);
        invertedEdges[v - 1].push_back(u - 1);
    }
    std::cout << MinNumOfAddedEdges(numVertices, edges, invertedEdges);
}