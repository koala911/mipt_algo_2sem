#include <iostream>
#include <string>
#include <vector>

typedef std::vector<int>::const_iterator Iterator;

class BigInteger {
public:
    BigInteger(int k = 0);
    explicit BigInteger(const std::string& s);
    explicit BigInteger(const std::vector<int>& nums);

    bool operator<(BigInteger const& other) const;
    bool operator>(BigInteger const& other) const;
    bool operator<=(BigInteger const& other) const;
    bool operator>=(BigInteger const& other) const;
    bool operator==(BigInteger const& other) const;
    bool operator!=(BigInteger const& other) const;

    BigInteger& operator*=(const BigInteger& other);
    BigInteger& operator-=(const BigInteger& other);
    BigInteger& operator+=(const BigInteger& other);
    BigInteger& operator%=(const BigInteger& other);
    BigInteger& operator/=(const BigInteger& other);

    BigInteger& operator++();
    const BigInteger operator++(int);
    BigInteger& operator--();
    const BigInteger operator--(int);

    BigInteger abs() const;

    const BigInteger operator-() const;
    bool isZero() const;

    const std::string toString() const;

    explicit operator bool();

    friend std::istream &operator>>(std::istream& in, BigInteger& number);
    friend std::ostream &operator<<(std::ostream& out, const BigInteger& number);

    friend const BigInteger operator*(const BigInteger& left, const BigInteger& right);
    friend const BigInteger operator/(const BigInteger& left, const BigInteger& right);
    friend const BigInteger operator%(const BigInteger& left, const BigInteger& right);
    friend const BigInteger operator+(const BigInteger& left, const BigInteger& right);
    friend const BigInteger operator-(const BigInteger& left, const BigInteger& right);

private:
    std::vector<int> digits;
    bool nonNegative = true;
    void addZero();
    void deleteZeros();
    void normalize();

    static void difference(BigInteger& left, const BigInteger& right);
    static void sum(BigInteger& left, const BigInteger& right);
    int compare(const BigInteger& other, bool sign = true) const;

    static void karatsubaAlgorithm(const Iterator& left_begin, const Iterator& left_end, const Iterator& right_begin, const Iterator& right_end, std::vector<int>& product);
};

BigInteger::BigInteger(int k) {
    if (k < 0) {
        nonNegative = false;
        k *= -1;
    } else if (k == 0) {
        digits.push_back(0);
        nonNegative = true;
    }
    while (k != 0) {
        digits.push_back(k % 10);
        k /= 10;
    }
}

BigInteger::BigInteger(const std::vector<int>& nums) {
    this->digits = nums;
    nonNegative = true;
}

BigInteger::BigInteger(const std::string& s) {
    nonNegative = true;
    for (size_t i = s.length() - 1; i > 0; --i) {
        digits.push_back(s[i] - '0');
    }
    if (s[0] == '-') {
        nonNegative = false;
    } else {
        digits.push_back(s[0] - '0');
    }
}

bool BigInteger::isZero() const {
    return (digits.size() == 1 && digits[0] == 0);
}

const BigInteger operator-(const BigInteger& left, const BigInteger& right) {
    BigInteger dif = left;
    return dif -= right;
}

const BigInteger operator+(const BigInteger& left, const BigInteger& right) {
    BigInteger sum = left;
    return sum += right;
}

void BigInteger::karatsubaAlgorithm(const Iterator& left_begin, const Iterator& left_end, const Iterator& right_begin, const Iterator& right_end, std::vector<int>& product) {
    size_t size = left_end - left_begin;
    if (size == 1) {
        product = {(*left_begin) * (*right_begin), 0};
        return;
    }
    if (left_end - left_begin == 0 || right_end - right_begin == 0) {
        return;
    }
    std::vector<int> product1(size, 0); // A_0 * B_0
    std::vector<int> product2(size, 0); // A_1 * B_1
    std::vector<int> product3(size, 0); // (A_0 + A_1) * (B_0 + B_1)
    karatsubaAlgorithm(left_begin, left_begin + size / 2, right_begin, right_begin + size / 2, product1);
    karatsubaAlgorithm(left_begin + size / 2, left_end, right_begin + size / 2, right_end, product2);
    std::vector<int> sum1(size / 2, 0);
    std::vector<int> sum2(size / 2, 0);
    for (size_t i = 0; i < size / 2; ++i) {
        sum1[i] += *(left_begin + i) + *(left_begin + i + size / 2);
        sum2[i] += *(right_begin + i) + *(right_begin + i + size / 2);
    }
    karatsubaAlgorithm(sum1.begin(), sum1.end(), sum2.begin(), sum2.end(), product3);
    for (size_t i = 0; i < size; ++i) {
        product[i] += product1[i];
        product[i + size / 2] += product3[i] - product1[i] - product2[i];
        product[i + size] += product2[i];
    }
}

const BigInteger operator*(const BigInteger& left, const BigInteger& right) {
    BigInteger result;
    if (left.isZero() || right.isZero()) {
        return result;
    }
    size_t powerOfTwo = 1;
    size_t size = std::max(left.digits.size(), right.digits.size());
    while (powerOfTwo < size) {
        powerOfTwo *= 2;
    }
    std::vector<int> leftDigits = left.digits;
    std::vector<int> rightDigits = right.digits;
    result.digits.resize(2 * powerOfTwo, 0);
    leftDigits.resize(powerOfTwo, 0);
    rightDigits.resize(powerOfTwo, 0);
    BigInteger::karatsubaAlgorithm(leftDigits.begin(), leftDigits.end(), rightDigits.begin(), rightDigits.end(), result.digits);
    result.normalize();
    result.deleteZeros();
    result.nonNegative = left.nonNegative == right.nonNegative;
    return result;
}

const BigInteger operator/(const BigInteger& left, const BigInteger& right) {
    BigInteger result;
    result.digits.resize(left.digits.size());
    BigInteger currentValue;

    for (int i = int(left.digits.size()) - 1; i >= 0; --i) {
        currentValue.addZero();
        currentValue.deleteZeros();
        currentValue.digits[0] = left.digits[i];
        int digit = 0;
        int leftDigit = 0, rightDigit = 9;
        while (leftDigit <= rightDigit) {
            int mid = (leftDigit + rightDigit) / 2;
            BigInteger current(0);
            current = right.abs() * mid;
            if (current.compare(currentValue, false) <= 0) {
                digit = mid;
                leftDigit = mid + 1;
            } else {
                rightDigit = mid - 1;
            }
        }
        result.digits[i] = digit;
        currentValue -= right.abs() * digit;
    }
    result.nonNegative = left.nonNegative == right.nonNegative;
    result.deleteZeros();
    return result;
}

const BigInteger operator%(const BigInteger& left, const BigInteger& right) {
    BigInteger result;
    result.digits.resize(left.digits.size());
    BigInteger currentValue;
    for (int i = int(left.digits.size()) - 1; i >= 0; --i) {
        currentValue.addZero();
        currentValue.deleteZeros();
        currentValue.digits[0] = left.digits[i];
        int digit = 0;
        int leftDigit = 0, rightDigit = 9;
        while (leftDigit <= rightDigit) {
            int mid = (leftDigit + rightDigit) / 2;
            BigInteger current;
            current = right.abs() * mid;
            if (current.compare(currentValue, false) <= 0) {
                digit = mid;
                leftDigit = mid + 1;
            } else {
                rightDigit = mid - 1;
            }
        }
        result.digits[i] = digit;
        currentValue = currentValue - right.abs() * digit;
    }
    currentValue.deleteZeros();
    currentValue.nonNegative = left.nonNegative;
    if (currentValue.digits.empty()) {
        currentValue.digits.push_back(0);
    }
    return currentValue;
}


std::istream& operator>>(std::istream& in, BigInteger& number) {
    std::string s;
    in >> s;
    number = BigInteger(s);
    return in;
}


std::ostream& operator<<(std::ostream& out, const BigInteger& number) {
    std::string s;
    s = number.toString();
    out << s;
    return out;
}


bool BigInteger::operator<(BigInteger const& other) const {
    return compare(other) == -1;
}


bool BigInteger::operator>(BigInteger const& other) const {
    return compare(other) == 1;
}


bool BigInteger::operator<=(BigInteger const& other) const {
    return (compare(other) <= 0);
}


bool BigInteger::operator>=(BigInteger const& other) const {
    return (compare(other) >= 0);
}


bool BigInteger::operator==(BigInteger const& other) const {
    return compare(other) == 0;
}


bool BigInteger::operator!=(BigInteger const& other) const {
    return compare(other) != 0;
}


BigInteger& BigInteger::operator*=(const BigInteger& other) {
    BigInteger result = *this;
    return *this = result * other;
}


BigInteger& BigInteger::operator+=(const BigInteger& other) {
    if ((nonNegative && other.nonNegative) || (!nonNegative && !other.nonNegative)) {
        sum(*this, other);
        return *this;
    }
    if ((nonNegative && !other.nonNegative) || (!nonNegative && other.nonNegative)) {
        if (this->compare(other, false) >= 0) {
            difference(*this, other);
            return *this;
        } else {
            BigInteger left = *this;
            BigInteger right = other;
            difference(right, left);
            return *this = right;
        }
    }
    return *this;
}


void BigInteger::addZero() {
    digits.insert(digits.begin(), 0);
}


BigInteger& BigInteger::operator%=(const BigInteger& other) {
    return *this = *this % other;
}


BigInteger& BigInteger::operator/=(const BigInteger& other) {
    BigInteger result = *this;
    return *this = result / other;
}


BigInteger& BigInteger::operator++() {
    if (nonNegative) {
        ++digits[0];
        size_t i = 0;
        while (i < digits.size() - 1 && digits[i] > 9) {
            ++digits[i + 1];
            digits[i] %= 10;
            ++i;
        }
        if (i == digits.size() - 1) {
            digits.back() %= 10;
            digits.push_back(1);
        }
        return *this;
    }
    --digits[0];
    size_t i = 0;
    while (i < digits.size() - 1 && digits[i] < 0) {
        --digits[i + 1];
        digits[i] += 10;
        ++i;
    }
    if (i == digits.size() - 1) {
        digits = {0};
        nonNegative = true;
    }
    return *this;
}


const BigInteger BigInteger::operator++(int) {
    BigInteger oldValue = *this;
    ++(*this);
    return oldValue;
}


BigInteger& BigInteger::operator--() {
    nonNegative = !nonNegative;
    ++(*this);
    nonNegative = !nonNegative;
    return *this;
}


const BigInteger BigInteger::operator--(int) {
    BigInteger oldValue = *this;
    --(*this);
    return oldValue;
}


BigInteger BigInteger::abs() const {
    BigInteger result = (*this);
    result.nonNegative = true;
    return result;
}


const BigInteger BigInteger::operator-() const {
    BigInteger result = *this;
    result.nonNegative = !this->nonNegative;
    if (digits.size() == 1 && digits[0] == 0) {
        result.nonNegative = true;
    }
    return result;
}


BigInteger& BigInteger::operator-=(const BigInteger& other) {
    if ((nonNegative && other.nonNegative) || (!nonNegative && !other.nonNegative)) {
        if (this->compare(other, false) >= 0) {
            difference(*this, other);
            return *this;
        } else {
            BigInteger left = *this;
            BigInteger right = other;
            difference(right, left);
            right.nonNegative = !right.nonNegative;
            return *this = right;
        }
    } else {
        sum(*this, other);
        return *this;
    }
}


const std::string BigInteger::toString() const {
    std::string result = nonNegative ? "" : "-";
    if (digits.size() == 1 && digits[0] == 0) {
        result = '0';
        return result;
    }
    bool forwardZeros = true;
    for (int i = int(digits.size()) - 1; i >= 0; --i) {
        if (digits[i] == 0 && forwardZeros) {
            continue;
        } else {
            forwardZeros = false;
            result += digits[i] + '0';
        }
    }
    if (result.empty() || (result.length() == 1 && result[0] == '-' )){
        result = '0';
    }
    return result;
}


BigInteger::operator bool() {
    return *this != 0;
}


void BigInteger::deleteZeros() {
    while (!digits.empty() && digits.back() == 0) {
        digits.pop_back();
    }
    if (digits.empty()) {
        nonNegative = true;
        digits.push_back(0);
    }
}

void BigInteger::normalize() {
    for (size_t i = 0; i < digits.size(); ++i) {
        if (digits[i] < 0 && digits[i] % 10) {
            digits[i + 1] += digits[i] / 10 - 1;
            digits[i] %= 10;
            digits[i] += 10;
        } else {
            digits[i + 1] += digits[i] / 10;
            digits[i] %= 10;
        }
    }
    while (digits.back() >= 10 || digits.back() < 0) {
        if (digits.back() < 0 && digits.back() % 10) {
            int temp = digits.back() / 10 - 1;
            digits.back() %= 10;
            digits.back() += 10;
            digits.push_back(temp);
        } else {
            int temp = digits.back() / 10;
            digits.back() %= 10;
            digits.push_back(temp);
        }
    }
}


void BigInteger::difference(BigInteger& left, const BigInteger& right) {
    int carry = 0;
    for (size_t i = 0; i < right.digits.size() || carry; ++i) {
        if (i == left.digits.size()) {
            left.digits.push_back(0);
        }
        left.digits[i] -= carry + (i < right.digits.size() ? right.digits[i] : 0);
        carry = left.digits[i] < 0;
        if (carry) left.digits[i] += 10;
    }
    left.deleteZeros();
}


void BigInteger::sum(BigInteger& left, const BigInteger& right) {
    int carry = 0;
    for (size_t i = 0; i < right.digits.size() || carry; ++i) {
        if (i == left.digits.size()) {
            left.digits.push_back(0);
        }
        left.digits[i] += carry + (i < right.digits.size() ? right.digits[i] : 0);
        carry = left.digits[i] / 10;
        left.digits[i] %= 10;
    }
    left.deleteZeros();
}


int BigInteger::compare(const BigInteger& other, bool sign) const {
    int check = 1;
    if (sign) {
        if (nonNegative && !other.nonNegative) {
            return 1;
        }
        if (!nonNegative && other.nonNegative) {
            return -1;
        }
        if (!nonNegative && !other.nonNegative) {
            check = -1;
        }
    }
    if (digits.size() < other.digits.size()) {
        return -check;
    }
    if (digits.size() > other.digits.size()) {
        return check;
    }
    for (size_t i(digits.size()); i > 0; --i) {
        if (digits[i - 1] < other.digits[i - 1]) return -check;
        if (digits[i - 1] > other.digits[i - 1]) return check;
    }
    return 0;
}

//----------------------------------------------------------------------------------------------------------------------

class Rational {
public:
    Rational(int k = 0);
    Rational(const BigInteger& other);

    Rational& operator+=(const Rational& other);
    Rational& operator-=(const Rational& other);
    Rational& operator*=(const Rational& other);
    Rational& operator/=(const Rational& other);
    const Rational operator-() const;

    bool operator<(const Rational& other) const;
    bool operator>(const Rational& other) const;
    bool operator<=(const Rational& other) const;
    bool operator>=(const Rational& other) const;
    bool operator==(const Rational& other) const;
    bool operator!=(const Rational& other) const;


    const std::string toString() const;
    const std::string asDecimal(size_t precision = 0) const;

    explicit operator double() const;

    friend const Rational operator+(const Rational& left, const Rational& right);
    friend const Rational operator-(const Rational& left, const Rational& right);
    friend const Rational operator*(const Rational& left, const Rational& right);
    friend const Rational operator/(const Rational& left, const Rational& right);

private:
    BigInteger numerator;
    BigInteger denominator;

    static BigInteger GCD(BigInteger& left, BigInteger& right);
};

Rational::Rational(int k): numerator(k), denominator(1) {}

Rational::Rational(const BigInteger& other): numerator(other), denominator(1) {}

BigInteger Rational::GCD(BigInteger& left, BigInteger& right) {
    // left >= right
    if (right.isZero()) {
        return left;
    }
    return GCD(right, left %= right);
}

Rational& Rational::operator+=(const Rational& other) {
    if (this == &other) {
        *this *= 2;
        return *this;
    }
    numerator *= other.denominator;
    numerator += denominator * other.numerator;
    denominator *= other.denominator;
    BigInteger left = numerator;
    BigInteger right = denominator;
    if (left < BigInteger(0)) {
        left = -left;
    }
    if (left < right) {
        std::swap(left, right);
    }
    BigInteger gcd = GCD(left, right);
    numerator /= gcd;
    denominator /= gcd;
    return *this;
}

Rational& Rational::operator-=(const Rational& other) {
    if (this == &other) {
        *this = 0;
        return *this;
    }
    numerator *= other.denominator;
    numerator -= denominator * other.numerator;
    denominator *= other.denominator;
    BigInteger left = numerator;
    BigInteger right = denominator;
    if (left < BigInteger(0)) {
        left = -left;
    }
    if (left < right) {
        std::swap(left, right);
    }
    BigInteger gcd = GCD(left, right);
    numerator /= gcd;
    denominator /= gcd;
    return *this;
}

const Rational operator+(const Rational& left, const Rational& right) {
    Rational result = left;
    result += right;
    return result;
}

const Rational operator-(const Rational& left, const Rational& right) {
    Rational result = left;
    result -= right;
    return result;
}

Rational& Rational::operator*=(const Rational& other) {
    if (this == &other) {
        numerator *= numerator;
        denominator *= denominator;
        return *this;
    }
    numerator *= other.numerator;
    denominator *= other.denominator;
    BigInteger left = numerator;
    BigInteger right = denominator;
    if (left < BigInteger(0)) {
        left = -left;
    }
    if (left < right) {
        std::swap(left, right);
    }
    BigInteger gcd = GCD(left, right);
    numerator /= gcd;
    denominator /= gcd;
    return *this;
}

const Rational operator*(const Rational& left, const Rational& right) {
    Rational result = left;
    result *= right;
    return result;
}

Rational& Rational::operator/=(const Rational& other) {
    if (this == &other) {
        *this = 1;
        return *this;
    }
    numerator *= other.denominator;
    denominator *= other.numerator;
    if (denominator < 0) {
        denominator = -denominator;
        numerator = -numerator;
    }
    BigInteger left = numerator;
    BigInteger right = denominator;
    if (left < BigInteger(0)) {
        left = -left;
    }
    if (left < right) {
        std::swap(left, right);
    }
    BigInteger gcd = GCD(left, right);
    numerator /= gcd;
    denominator /= gcd;
    return *this;
}

const Rational operator/(const Rational& left, const Rational& right) {
    Rational result = left;
    result /= right;
    return result;
}

const Rational Rational::operator-() const {
    Rational result = *this;
    result.numerator = -numerator;
    return result;
}

bool Rational::operator<(const Rational& other) const {
    return numerator * other.denominator < other.numerator * denominator;
}

bool Rational::operator<=(const Rational& other) const {
    return numerator * other.denominator <= other.numerator * denominator;
}

bool Rational::operator>(const Rational& other) const {
    return numerator * other.denominator > other.numerator * denominator;
}

bool Rational::operator>=(const Rational& other) const {
    return numerator * other.denominator >= other.numerator * denominator;
}

bool Rational::operator==(const Rational& other) const {
    return numerator * other.denominator == other.numerator * denominator;
}

bool Rational::operator!=(const Rational& other) const {
    return !(*this == other);
}

const std::string Rational::toString() const {
    std::string result = numerator.toString();
    if (denominator.abs() > BigInteger(1)) {
        result.push_back('/');
        result.append(denominator.toString());
    }
    return result;
}

const std::string Rational::asDecimal(size_t precision) const {
    if (*this == 0) {
        if (precision == 0) {
            return "0";
        }
        std::vector<int> zeros(precision, '0');
        return "0." + std::string(zeros.begin(), zeros.end());
    }
    std::vector<int> powerOfTen(precision + 1, 0);
    powerOfTen.back() = 1;
    BigInteger temp = numerator * BigInteger(powerOfTen);
    temp /= denominator;
    std::string result = temp.toString();
    if (precision > 0) {
        if (int(result.size()) - int(precision) <= 1 && result[0] == '-' ) {
            std::vector<int> zeros(2 + precision - result.size(), '0');
            result = '-' + std::string(zeros.begin(), zeros.end()) + std::string(result.begin() + 1, result.end());
        } else if (int(result.size()) - int(precision) <= 0) {
            std::vector<int> zeros(1 + precision - result.size(), '0');
            result = std::string(zeros.begin(), zeros.end()) + result;
        }
        result.insert(result.end() - precision, '.');
    }
    return result;
}

Rational::operator double() const {
    double result = 0;
    std::string stringDouble = asDecimal(8);
    int sign = 1;
    if (stringDouble[0] == '-') {
        sign = -1;
    }
    size_t i = 0;
    if (sign < 0) {
        ++i;
    }
    size_t multiplier = 1;
    while (i < stringDouble.size() && stringDouble[i] != '.') {
        ++i;
    }
    if (i < stringDouble.size()) {
        size_t j = i + 1;
        while (j < stringDouble.size()) {
            multiplier *= 10;
            result += (stringDouble[j] - '0') / double(multiplier);
            ++j;
        }
    }
    multiplier = 1;
    --i;
    while (i > 0) {
        result += multiplier * (stringDouble[i] - '0');
        multiplier *= 10;
        --i;
    }
    if (sign > 0) {
        result += multiplier * (stringDouble[0] - '0');
    }
    return result * sign;
}
