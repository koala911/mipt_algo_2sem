#include <algorithm>
#include <iterator>
#include <type_traits>


template <typename T>
class List {
public:
    typedef T value_type;

    List();
    explicit List(size_t count, const T& value = T());
    List(const List<T>& other);
    List(List<T>&& other);

    List<T>& operator=(const List<T>& other);
    List<T>& operator=(List<T>&& other);

    ~List();

    size_t size() const;

    T& front();
    T& back();

    void clear();
    bool empty();

    struct Node {
        T* value;
        Node* next;
        Node* prev;
        explicit Node(Node* next = nullptr, Node* prev = nullptr): value(nullptr), next(next), prev(prev) {}
        //explicit Node(const T& value, Node* next = nullptr, Node* prev = nullptr): value(nullptr), next(next), prev(prev) {}
        //explicit Node(T&& value, Node* next = nullptr, Node* prev = nullptr): value(nullptr), next(next), prev(prev) {}
        template <typename... Args>
        explicit Node(Node* next, Node* prev, Args&&... args): value(new T(std::forward<Args>(args)...)), next(next), prev(prev){}
        ~Node() {
            delete value;
        }
    };

    template <bool Const>
    struct MyIterator: public std::iterator<std::bidirectional_iterator_tag, T> {
        typedef T value_type;
        Node* node;

        explicit MyIterator(Node* node = nullptr): node(node) {}

        MyIterator(const MyIterator& other): node(other.node) {}

        MyIterator& operator=(const MyIterator& other) {
            if (this == &other) {
                return *this;
            }
            node = other.node;
            return *this;
        }

        template <bool Const_ = Const>
        std::enable_if_t<!Const_, T&>
        operator*() {
            return *(node->value);
        }

        template <bool Const_ = Const>
        std::enable_if_t<Const_, const T&>
        operator*() const {
            return *(node->value);
        }

        MyIterator& operator++() {
            node = node->next;
            return *this;
        }

        MyIterator operator++(int) {
            iterator old(node);
            node = node->next;
            return old;
        }

        MyIterator& operator--() {
            node = node->prev;
            return *this;
        }

        MyIterator operator--(int) {
            iterator old(node);
            node = node->prev;
            return old;
        }

        bool operator==(const MyIterator& other) {
            return (node == other.node);
        }

        bool operator!=(const MyIterator& other) {
            return (node != other.node);
        }

        operator MyIterator<true>() const {
            return MyIterator<true>(node);
        }

        operator MyIterator<false>() const {
            return MyIterator<false>(node);
        }
    };

    typedef MyIterator<false> iterator;
    typedef MyIterator<true> const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    iterator begin() const ;
    iterator end() const ;
    const_iterator cbegin() const;
    const_iterator cend() const;

    const_iterator insert(const_iterator it, const T& value);
    const_iterator insert(const_iterator it, T&& value);
    template<typename InputIter>
    const_iterator insert(const_iterator it, InputIter first, InputIter last);

    void erase(iterator it);
    void erase(iterator first, iterator last);

    void push_front(const T& value);
    void push_front(T&& value);
    void pop_front();
    void push_back(const T& value);
    void push_back(T&& value);
    void pop_back();

    template <typename... Args>
    void emplace(const_iterator it, Args&&... args);
    template <typename... Args>
    void emplace_front(Args&&... args);
    template <typename... Args>
    void emplace_back(Args&&... args);

    void reverse();
    void unique();

private:

    Node* head;
    Node* tail;
    size_t length;
};


template <typename T>
List<T>::List(): tail(new Node) {
    head = tail;
    length = 0;
}

template <typename T>
List<T>::List(size_t count, const T& value): tail(new Node) {
    head = tail;
    length = 0;
    for (size_t i = 0; i < count; ++i) {
        push_back(value);
    }
}

template <typename T>
List<T>::List(const List<T>& other): tail(new Node) {
    head = tail;
    length = 0;
    for (auto value: other) {
        push_back(value);
    }
}

template <typename T>
List<T>::List(List<T>&& other) {
    length = other.length;
    head = other.head;
    tail = other.tail;
    other.head = new Node;
    other.tail = other.head;
    other.length = 0;
}

template <typename T>
List<T>& List<T>::operator=(const List<T>& other) {
    if (this == &other) {
        return *this;
    }
    clear();
    for (auto value: other) {
        push_back(value);
    }
    return *this;
}

template <typename T>
List<T>& List<T>::operator=(List<T>&& other) {
    if (this == &other) {
        return *this;
    }
    clear();
    tail = other.tail;
    head = other.head;
    length = other.length;
    other.head = new Node;
    other.tail = other.head;
    other.length = 0;
    return *this;
}

template <typename T>
List<T>::~List() {
    Node* current = head;
    while (current != nullptr) {
        Node* next = current->next;
        delete current;
        current = next;
    }
}

template <typename T>
size_t List<T>::size() const {
    return length;
}

template <typename T>
T& List<T>::front() {
    return *(begin());
}

template <typename T>
T& List<T>::back() {
    return *(tail->prev->value);
}

template <typename T>
void List<T>::clear() {
    Node* current = head;
    while (current != nullptr) {
        Node* next = current->next;
        delete current;
        current = next;
    }
    tail = new Node;
    head = tail;
    length = 0;
}

template <typename T>
bool List<T>::empty() {
    return (head == tail);
}

template <typename T>
typename List<T>::iterator List<T>::begin() const {
    return iterator(head);
}

template <typename T>
typename List<T>::iterator List<T>::end() const {
    return iterator(tail);
}

template <typename T>
typename List<T>::const_iterator List<T>::cbegin() const {
    return const_iterator(head);
}

template <typename T>
typename List<T>::const_iterator List<T>::cend() const {
    return const_iterator(tail);
}

template <typename T>
typename List<T>::const_iterator List<T>::insert(const_iterator it, const T& value) {
    auto node = it.node;
    iterator result(nullptr);
    if (node == head) {
        auto new_node = new Node(head);
        new_node->value = new T(value);
        head->prev = new_node;
        head = new_node;
        result.node = new_node;
    } else {
        auto new_node = new Node(node, node->prev);
        new_node->value = new T(value);
        node->prev->next = new_node;
        node->prev = new_node;
        result.node = new_node;
    }
    ++length;
    return result;
}

template <typename T>
typename List<T>::const_iterator List<T>::insert(const_iterator it, T&& value) {
    auto node = it.node;
    iterator result(nullptr);
    if (node == head) {
        auto new_node = new Node(head);
        new_node->value = new T(std::move(value));
        head->prev = new_node;
        head = new_node;
        result.node = new_node;
    } else {
        auto new_node = new Node(node, node->prev);
        new_node->value = new T(std::move(value));
        node->prev->next = new_node;
        node->prev = new_node;
        result.node = new_node;
    }
    ++length;
    return result;
}

template <typename T>
template <typename InputIter>
typename List<T>::const_iterator List<T>::insert(const_iterator it, InputIter first, InputIter last) {
    for (auto current = first; current != last; ++current) {
        it = insert(it, *current);
    }
    return it;
}

template <typename T>
void List<T>::erase(iterator it) {
    auto node = it.node;
    if (node == head) {
        auto next = head->next;
        next->prev = nullptr;
        delete head;
        head = next;
    } else {
        auto next = node->next;
        auto prev = node->prev;
        next->prev = prev;
        prev->next = next;
        delete node;
    }
    --length;
}

template <typename T>
void List<T>::erase(iterator first, iterator last) {
    for (auto it = first; it != last; ++it) {
        erase(it);
    }
}

template <typename T>
void List<T>::push_front(const T& value) {
    insert(cbegin(), value);
}

template <typename T>
void List<T>::push_front(T&& value) {
    insert(cbegin(), std::move(value));
}

template <typename T>
void List<T>::pop_front() {
    erase(begin());
}

template <typename T>
void List<T>::push_back(const T& value) {
    insert(cend(), value);
}

template <typename T>
void List<T>::push_back(T&& value) {
    insert(cend(), std::move(value));
}

template <typename T>
void List<T>::pop_back() {
    erase(iterator(tail->prev));
}

template <typename T>
template <typename... Args>
void List<T>::emplace(const_iterator it, Args&&... args) {
    auto node = it.node;
    if (node == head) {
        auto new_node = new Node(head, nullptr, std::forward<Args>(args)...);
        head->prev = new_node;
        head = new_node;
    } else {
        auto new_node = new Node(node, node->prev, std::forward<Args>(args)...);
        node->prev->next = new_node;
        node->prev = new_node;
    }
    ++length;
}

template <typename T>
template <typename... Args>
void List<T>::emplace_front(Args&&... args) {
    emplace(cbegin(), std::forward<Args>(args)...);
}

template <typename T>
template <typename... Args>
void List<T>::emplace_back(Args&&... args) {
    emplace(cend(), std::forward<Args>(args)...);
}

template <typename T>
void List<T>::reverse() {
    List<T> other(*this);
    clear();
    size_t size = other.size();
    for (size_t i = 0; i < size; ++i) {
        push_back(other.back());
        other.pop_back();
    }
}

template <typename T>
void List<T>::unique() {
    auto it = begin();
    auto curr = it;
    ++curr;
    auto prev = it;
    while (curr != end()) {
        auto next = curr;
        ++next;
        if (*prev == *curr) {
            erase(curr);
        } else {
            prev = curr;
        }
        curr = next;
    }
}
