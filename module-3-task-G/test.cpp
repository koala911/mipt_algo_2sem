#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "list.h"

TEST_CASE("Test push_back") {
    INFO("TEST_CASE:Test push_back started!");
    List<int> list;
    list.push_back(1);
    list.push_back(12);
    REQUIRE(list.size(), 2);
}

TEST_CASE("Test push_front") {
    INFO("TEST_CASE:Test push_front started!");
    List<int> list;
    list.push_front(1);
    list.push_front(243);
    list.push_front(1332);
    REQUIRE(list.size(), 3);
}

TEST_CASE("Test pop_front") {
INFO("TEST_CASE:Test pop_front started!");
    List<int> list;
    list.push_front(1);
    list.push_front(243);
    list.push_back(1332);
    list.pop();
    list.pop();
    list.pop();
    REQUIRE(list.size(), 0);
}