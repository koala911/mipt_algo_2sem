#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    int numOfVertices = 0;
    int numOfEdges = 0;
    std::cin >> numOfVertices >> numOfEdges;
    std::vector<Edge> edges;
    for (int i = 0; i < numOfEdges; ++i) {
        int from = 0;
        int to = 0;
        int cost = 0;
        std::cin >> from >> to >> cost;
        edges.emplace_back(from - 1, to - 1, cost);
    }
    std::cout << MST(numOfVertices, edges);
}
