#include "MST.h"


int MST(int numOfVertices, std::vector<Edge>& edges) {
    std::vector<int> colors(numOfVertices);
    for (int i = 0; i < numOfVertices; ++i) {
        colors[i] = i;
    }
    std::sort(edges.begin(), edges.end());
    int currentWeight = 0;
    for (const Edge& edge: edges) {
        int from = edge.from;
        int to = edge.to;
        int cost = edge.cost;
        if (colors[from] != colors[to]) {
            currentWeight += cost;
            int oldColor = colors[from];
            int newColor = colors[to];
            for (int& color: colors) {
                if (color == oldColor) {
                    color = newColor;
                }
            }
        }
    }
    return currentWeight;
}
