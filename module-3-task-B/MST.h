#include <vector>

struct Edge {
    int from;
    int to;
    int cost;
    Edge(int from, int to, int cost): from(from), to(to), cost(cost) {}
    bool operator<(const Edge& other) const {
        return cost < other.cost;
    }
};

int MST(int numOfVertices, std::vector<Edge>& edges);