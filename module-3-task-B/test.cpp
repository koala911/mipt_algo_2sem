#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "MST.h"

TEST_CASE("Test example") {
    INFO("TEST_CASE:Test example started!");
    int numOfVertices = 2;
    std::vector<Edge> edges = {Edge(0, 1, 10986)};
    REQUIRE(MST(numOfVertices, edges) == 10986);
}

TEST_CASE("Test K_3") {
    INFO("TEST_CASE:Test K_3 started!");
    int numOfVertices = 3;
    std::vector<Edge> edges = {Edge(0, 1, 1), Edge(1, 2, 1), Edge(2, 0, 1)};
    REQUIRE(MST(numOfVertices, edges) == 2);
}

TEST_CASE("Test simple graph") {
    INFO("TEST_CASE:Test simple graph started!");
    int numOfVertices = 4;
    std::vector<Edge> edges = {Edge(0, 1, 1), Edge(1, 2, 2), Edge(2, 3, 3), Edge(3, 0, 4)};
    REQUIRE(MST(numOfVertices, edges) == 6);
}