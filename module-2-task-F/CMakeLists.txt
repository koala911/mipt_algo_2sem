cmake_minimum_required(VERSION 3.15)
project(module_2_task_F)

set(CMAKE_CXX_STANDARD 14)

find_package(Catch2 REQUIRED)
add_executable(module_2_task_F geometry.h test.cpp)
target_link_libraries(module_2_task_F asan)
target_link_libraries(module_2_task_F Catch2::Catch2)

include(CTest)
include(Catch)
catch_discover_tests(module_2_task_F)