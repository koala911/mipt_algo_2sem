#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "geometry.h"
#include <vector>

TEST_CASE("Test Line") {
    INFO("TEST_CASE:Test Line started!");
    Line line1(Point(1, 2), Point(2, 3));
    Line line2(Point(0, 1), Point(1, 2));
    Line line2(Point(1, 2), Point(2, 1));
    REQUIRE(line1 == line2);
    REQUIRE(line1 != line2);
}

TEST_CASE("Test Polygon") {
    INFO("TEST_CASE:Test Polygon started!");
    Point a(1, 2), b(1, -2), c(-1, -2), d(-1, 2);
    Polygon p1(a, b, c, d);
    Polygon p2(d, c, b, a);
    Polygon p3(a, b, d);
    REQUIRE(p1 == p2);
    REQUIRE(p1 != p3);
}

TEST_CASE("Test Ellipse") {
    INFO("TEST_CASE:Test Ellipse started!");
    Point a(1, 2), b(1, -2), c(-1, -2), d(-1, 2);
    Ellipse ellipse1(a, b, 4);
    Ellipse ellipse2(c, d, 4);
    REQUIRE(ellipse1.isCongruentTo(ellipse2));
}
