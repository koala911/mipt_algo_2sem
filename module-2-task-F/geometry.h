#pragma once

#include <cmath>
#include <vector>
#include <typeinfo>
#include <stdexcept>
#include <algorithm>


const double EPS = 1e-6;
const double PI = 3.14159265;

// |a b|
// |c d|
double Det(double a, double b, double c, double d) {
    return a * d - b * c;
}

class Vector {
public:
    explicit Vector(double x_ = 0, double y_ = 0): x(x_), y(y_) {}

    double GetX() const;
    double GetY() const;
    double Dot(const Vector& other) const;
    double Area(const Vector& other) const;
    bool IsCollinear(const Vector& other) const;
    double Abs() const;
    Vector operator*(double alpha) const;
    Vector operator/(double alpha) const;
    Vector operator+(const Vector& other) const;
    Vector Orthogonal() const;
    Vector Rotate(double sin, double cos) const;
private:
    double x;
    double y;
};

double Vector::GetX() const {
    return x;
}

double Vector::GetY() const {
    return y;
}

double Vector::Dot(const Vector& other) const {
    return x * other.x + y * other.y;
}

double Vector::Area(const Vector& other) const {
    return Det(x, y, other.x, other.y);
}

bool Vector::IsCollinear(const Vector& other) const {
    return (std::abs(std::abs(this->Dot(other)) - Abs() * other.Abs()) < EPS);
}

double Vector::Abs() const {
    return sqrt(x * x + y * y);
}

Vector Vector::operator*(double alpha) const {
    return Vector(alpha * x, alpha * y);
}

Vector Vector::operator/(double alpha) const {
    return Vector(x / alpha, y / alpha);
}

Vector Vector::operator+(const Vector& other) const {
    return Vector(x + other.x, y + other.y);
}

Vector Vector::Orthogonal() const {
    return Vector(y, -x);
}

Vector Vector::Rotate(double sin, double cos) const {
    double newX = x * cos - y * sin;
    double newY = x * sin + y * cos;
    return Vector(newX, newY);
}

//----------------------------------------------------------------------------------------------------------------------

class Line;

struct Point {
    double x;
    double y;
    explicit Point(double x_ = 0, double y_ = 0): x(x_), y(y_) {}

    Vector operator-(const Point& other) const;
    Point operator+(const Vector& other) const;
    Point operator-(const Vector& other) const;
    bool operator==(const Point& other) const;
    bool operator!=(const Point& other) const;
    void Scale(const Point& center, double coefficient);
    void Reflex(const Point& center);
    void Reflex(const Line& axis);
    void Rotate(const Point& center, double angle);
    double Dist(const Point& other) const;
};

Vector Point::operator-(const Point& other) const {
    Vector result(x - other.x, y - other.y);
    return result;
}

Point Point::operator+(const Vector& other) const {
    Point result(x + other.GetX(), y + other.GetY());
    return result;
}

Point Point::operator-(const Vector& other) const {
    Point result(x - other.GetX(), y - other.GetY());
    return result;
}

bool Point::operator==(const Point& other) const {
    return (std::abs(x - other.x) < EPS && (y - other.y) < EPS);
}

bool Point::operator!=(const Point& other) const {
    return !(*this == other);
}

double Point::Dist(const Point &other) const {
    return (*this - other).Abs();
}

void Point::Scale(const Point& center, double coefficient) {
    Vector shift = (*this - center) * coefficient;
    *this = center + shift;
}

void Point::Reflex(const Point& center) {
    Vector shift = *this - center;
    *this = center - shift;
}

void Point::Rotate(const Point& center, double angle) {
    double sin = std::sin(angle);
    double cos = std::cos(angle);
    Vector shift = (*this - center).Rotate(sin, cos);
    *this = center + shift;
}

Point GetMiddle(const Point& first, const Point& second) {
    return first + (second - first) / 2;
}

//----------------------------------------------------------------------------------------------------------------------

class Line {
public:
    Line(const Point& point1, const Point& point2);
    Line(double k, double b);
    Line(const Point& point, double k);

    bool operator==(const Line& other) const;
    bool operator!=(const Line& other) const;
    Point Intersection(const Line& other) const;
    double Dist(const Point& point) const;
    Vector Normal() const;
    Vector Directive() const;
    Point GetFirstPoint() const;
    Point GetSecondPoint() const;
private:
    Point first;
    Point second;
};

Line::Line(const Point& point1, const Point& point2): first(point1), second(point2) {}

Line::Line(const Point& point, double k) {
    first = point;
    second = point;
    second.x += 1;
    second.y += k;
}

Line::Line(double k, double b) {
    first = Point(0, k);
    second = Point(1, k + b);
}

bool Line::operator==(const Line& other) const {
    return ((second - first).IsCollinear(other.second - other.first) &&
    (second - other.first).IsCollinear(other.second - other.first));
}

bool Line::operator!=(const Line &other) const {
    return !(*this == other);
}

Point Line::Intersection(const Line& other) const {
    //решаю систему first + (second - first) * t = other.first + (other.second - other.first) * t' методом Крамера
    double t = Det(other.first.x - first.x, other.first.x - other.second.x,
            other.first.y - first.y, other.first.y - other.second.y) / Det(second.x - first.x,
                    other.first.x - other.second.x, second.y - first.y, other.first.y - other.second.y);
    return first + (second - first) * t;
}

double Line::Dist(const Point& point) const {
    double area = std::abs((second - first).Area(point - first));
    return area / (second - first).Abs();
}

Vector Line::Directive() const {
    return second - first;
}

Vector Line::Normal() const {
    return Directive().Orthogonal();
}

Point Line::GetFirstPoint() const {
    return first;
}

Point Line::GetSecondPoint() const {
    return second;
}

Line GetMidPerpendicular(const Point& first, const Point& second) {
    Vector perpendicular = (second - first).Orthogonal();
    Point center = GetMiddle(first, second);
    return Line(center, center + perpendicular);
}

Line GetBisector(const Point& angularPoint, const Point& pointOnFirstSide, const Point& pointOnSecondSide) {
    Vector firstSide = pointOnFirstSide - angularPoint;
    Vector secondSide = pointOnSecondSide - angularPoint;
    Vector bisectorVector = firstSide / firstSide.Abs() + secondSide / secondSide.Abs();
    return Line(angularPoint, angularPoint + bisectorVector);
}

void Point::Reflex(const Line& axis) {
    Vector normal = axis.Normal();
    Point pointOnAxis = axis.GetFirstPoint();
    *this = *this + normal * (2 * (pointOnAxis - *this).Dot(normal) / (normal.Abs() * normal.Abs()));
}

//----------------------------------------------------------------------------------------------------------------------

class Shape {
public:
    virtual double Area() const = 0;
    virtual double Perimeter() const = 0;
    virtual bool operator==(const Shape& another) const = 0;
    bool operator!=(const Shape& another) const;
    virtual bool IsCongruentTo(const Shape& another) const = 0;
    virtual bool IsSimilarTo(const Shape& another) const = 0;
    virtual bool ContainsPoint(Point point) const = 0;
    virtual void Rotate(Point center, double angle) = 0;
    virtual void Reflex(Point center) = 0;
    virtual void Reflex(Line axis) = 0;
    virtual void Scale(Point center, double coefficient) = 0;
    virtual ~Shape() = default;
};

bool Shape::operator!=(const Shape &another) const {
    return !(*this == another);
}

//----------------------------------------------------------------------------------------------------------------------

class Polygon: public Shape {
public:
    explicit Polygon(const std::vector<Point>& points_ = std::vector<Point>()): points(points_) {};
    template <typename Head, typename... Tail>
    explicit Polygon(const Head& head, const Tail&... tail);

    int verticesCount() const;
    const std::vector<Point>& getVertices() const;
    bool isConvex() const;
    double Area() const override;
    double Perimeter() const override;
    bool operator==(const Shape& another) const override;
    bool IsCongruentTo(const Shape& another) const override;
    bool IsSimilarTo(const Shape& another) const override;
    bool ContainsPoint(Point point) const override;
    void Rotate(Point center, double angle) override;
    void Reflex(Point center) override;
    void Reflex(Line axis) override;
    void Scale(Point center, double coefficient) override;

    ~Polygon() override = default;

protected:
    std::vector<Point> points;

private:
    template <typename Head, typename... Tail>
    void putPoints(const Head& head, const Tail&... tail);
    void putPoints() {}

    mutable std::vector<Point> pointsToCompare;
    mutable std::vector<Point> otherPoints;
    bool isCongruentToOtherVector() const;
    bool isSimilarToOtherVector() const;
};

template <typename Head, typename... Tail>
Polygon::Polygon(const Head& head, const Tail&... tail) {
    putPoints(head, tail...);
}

template <typename Head, typename... Tail>
void Polygon::putPoints(const Head& head, const Tail&... tail) {
    points.push_back(head);
    putPoints(tail...);
}

int Polygon::verticesCount() const {
    return points.size();
}

const std::vector<Point> & Polygon::getVertices() const {
    return points;
}

bool Polygon::isConvex() const {
    int numOfPoints = points.size();
    if (numOfPoints < 4) {
        return true;
    }
    bool alwaysNonPositive = true;
    bool alwaysNonNegative = true;
    for (int i = 0; i < numOfPoints; ++i) {
        Vector current = points[(i + 1) % numOfPoints] - points[i];
        Vector next = points[(i + 2) % numOfPoints] - points[(i + 1) % numOfPoints];
        if (current.Area(next) > 0) {
            alwaysNonPositive = false;
        }
        if (current.Area(next) < 0) {
            alwaysNonNegative = false;
        }
    }
    return alwaysNonPositive || alwaysNonNegative;
}

double Polygon::Area() const {
    double totalArea = 0;
    for (int i = 0; i < points.size() - 1; ++i) {
        totalArea += (points[i + 1] - points[0]).Area(points[i] - points[0]);
    }
    return std::abs(totalArea / 2);
}

double Polygon::Perimeter() const {
    double totalPerimeter = 0;
    for (int i = 0; i < points.size(); ++i) {
        totalPerimeter += points[i].Dist(points[(i + 1) % points.size()]);
    }
    return totalPerimeter;
}

bool Polygon::operator==(const Shape& another) const {
    try {
        const Polygon& other = dynamic_cast<const Polygon&>(another);
        if (points.size() != other.points.size()) {
            return false;
        }
        for (int startIndex = 0; startIndex < points.size(); ++startIndex) {
            bool wasDifferentPoints = false;
            for (int i = 0; i < points.size(); ++i) {
                if (points[(startIndex + i) % points.size()] != other.points[i]) {
                    wasDifferentPoints = true;
                    break;
                }
            }
            if (!wasDifferentPoints) {
                return true;
            }
        }
        std::vector<Point> reversePoints(points.begin(), points.end());
        std::reverse(reversePoints.begin(), reversePoints.end());
        for (int startIndex = 0; startIndex < points.size(); ++startIndex) {
            bool wasDifferentPoints = false;
            for (int i = 0; i < points.size(); ++i) {
                if (reversePoints[(startIndex + i) % points.size()] != other.points[i]) {
                    wasDifferentPoints = true;
                    break;
                }
            }
            if (!wasDifferentPoints) {
                return true;
            }
        }
        return false;
    } catch (const std::bad_cast& exception) {
        return false;
    }
}

void Polygon::Scale(Point center, double coefficient) {
    for (Point& point: points) {
        point.Scale(center, coefficient);
    }
}

void Polygon::Reflex(Line axis) {
    for (Point& point: points) {
        point.Reflex(axis);
    }
}

void Polygon::Reflex(Point center) {
    for (Point& point: points) {
        point.Reflex(center);
    }
}

void Polygon::Rotate(Point center, double angle) {
    for (Point& point: points) {
        point.Rotate(center, angle);
    }
}

bool Polygon::isCongruentToOtherVector() const {
    for (int startIndex = 0; startIndex < pointsToCompare.size(); ++startIndex) {
        bool wasDifference = false;
        for (int i = 0; i < pointsToCompare.size(); ++i) {
            Vector side = pointsToCompare[(i + 1 + startIndex) % pointsToCompare.size()] - pointsToCompare[(i + startIndex) % pointsToCompare.size()];
            Vector nextSide = pointsToCompare[(i + 2 + startIndex) % pointsToCompare.size()] - pointsToCompare[(i + 1 + startIndex) % pointsToCompare.size()];
            Vector otherSide = otherPoints[(i + 1) % otherPoints.size()] - otherPoints[i];
            Vector nextOtherSide = otherPoints[(i + 2) % points.size()] - otherPoints[(i + 1) % otherPoints.size()];
            double dist = side.Abs();
            double otherDist = otherSide.Abs();
            double area = side.Area(nextSide);
            double otherArea = otherSide.Area(nextOtherSide);
            double scalar = side.Dot(nextSide);
            double otherScalar = otherSide.Dot(nextOtherSide);
            if (std::abs(dist - otherDist) >= EPS ||
                std::abs(area - otherArea) >= EPS ||
                std::abs(scalar - otherScalar) >= EPS) {
                wasDifference = true;
                break;
            }
        }
        if (!wasDifference) {
            return true;
        }
    }
    return false;
}

bool Polygon::IsCongruentTo(const Shape& another) const {
    try {
        const Polygon& other = dynamic_cast<const Polygon&>(another);
        if (points.size() != other.points.size()) {
            return false;
        }
        pointsToCompare = points;
        otherPoints = other.points;
        if (isCongruentToOtherVector()) {
            return true;
        }
        std::reverse(pointsToCompare.begin(), pointsToCompare.end());
        if (isCongruentToOtherVector()) {
            return true;
        }
        return false;
    } catch (const std::bad_cast& exception) {
        return false;
    }
}

bool Polygon::isSimilarToOtherVector() const {
    for (int startIndex = 0; startIndex < pointsToCompare.size(); ++startIndex) {
        bool wasDifference = false;
        double previousRatio = -1;
        for (int i = 0; i < pointsToCompare.size(); ++i) {
            Vector side = pointsToCompare[(i + 1 + startIndex) % pointsToCompare.size()] - pointsToCompare[(i + startIndex) % pointsToCompare.size()];
            Vector nextSide = pointsToCompare[(i + 2 + startIndex) % pointsToCompare.size()] - pointsToCompare[(i + 1 + startIndex) % pointsToCompare.size()];
            Vector otherSide = otherPoints[(i + 1) % otherPoints.size()] - otherPoints[i];
            Vector nextOtherSide = otherPoints[(i + 2) % otherPoints.size()] - otherPoints[(i + 1) % otherPoints.size()];
            double dist = side.Abs();
            double otherDist = otherSide.Abs();
            double ratio = dist / otherDist;
            if (previousRatio == -1) {
                previousRatio = ratio;
            }
            double area = side.Area(nextSide);
            double otherArea = otherSide.Area(nextOtherSide);
            double scalar = side.Dot(nextSide);
            double otherScalar = otherSide.Dot(nextOtherSide);
            if (std::abs(ratio - previousRatio) >= EPS ||
                std::abs(area - otherArea * ratio * ratio) >= EPS ||
                std::abs(scalar - otherScalar * ratio * ratio) >= EPS) {
                wasDifference = true;
                break;
            }
        }
        if (!wasDifference) {
            return true;
        }
    }
}

bool Polygon::IsSimilarTo(const Shape& another) const {
    try {
        const Polygon& other = dynamic_cast<const Polygon&>(another);
        if (points.size() != other.points.size()) {
            return false;
        }
        pointsToCompare = points;
        otherPoints = other.points;
        if (isSimilarToOtherVector()) {
            return true;
        }
        std::reverse(pointsToCompare.begin(), pointsToCompare.end());
        if (isSimilarToOtherVector()) {
            return true;
        }
        return false;
    } catch (const std::bad_cast& exception) {
        return false;
    }
}

bool Polygon::ContainsPoint(Point point) const {
    int numOfPoints = points.size();
    bool alwaysNonPositive = true;
    bool alwaysNonNegative = true;
    for (int i = 0; i < numOfPoints; ++i) {
        Vector side = points[(i + 1) % numOfPoints] - points[i];
        if (side.Area(point - points[i]) > 0) {
            alwaysNonPositive = false;
        }
        if (side.Area(point - points[i]) < 0) {
            alwaysNonNegative = false;
        }
    }
    return alwaysNonPositive || alwaysNonNegative;
}

//----------------------------------------------------------------------------------------------------------------------

class Ellipse: public Shape {
public:
    Ellipse(const Point& first, const Point& second, double dist): firstFocus(first), secondFocus(second), width(dist) {}

    std::pair<Point, Point> focuses() const;
    std::pair<Line, Line> directrices() const;
    double Eccentricity() const;
    Point Center() const;
    double Area() const override;
    double Perimeter() const override;
    bool operator==(const Shape& another) const override;
    bool IsCongruentTo(const Shape& another) const override;
    bool IsSimilarTo(const Shape& another) const override;
    bool ContainsPoint(Point point) const override;
    void Rotate(Point center, double angle) override;
    void Reflex(Point center) override;
    void Reflex(Line axis) override;
    void Scale(Point center, double coefficient) override;

    ~Ellipse() override = default;

protected:
    Point firstFocus;
    Point secondFocus;
    double width; // 2 * a
};

std::pair<Point, Point> Ellipse::focuses() const {
    return {firstFocus, secondFocus};
}

Point Ellipse::Center() const {
    return Point((firstFocus.x + secondFocus.x) / 2, (firstFocus.y + secondFocus.y) / 2);
}

std::pair<Line, Line> Ellipse::directrices() const {
    Vector bigAxis = firstFocus - secondFocus;
    bigAxis = bigAxis * (width / (2 * bigAxis.Abs() * Eccentricity()));
    Point point1 = center() + bigAxis;
    Vector smallAxis = bigAxis.Orthogonal();
    Point point2 = point1 + smallAxis;
    Line directrice1(point1, point2);
    point1 = center() - bigAxis;
    point2 = point1 + smallAxis;
    Line directrice2(point1, point2);
    return {directrice1, directrice2};
}

double Ellipse::Eccentricity() const {
    return firstFocus.Dist(secondFocus) / width;
}

bool Ellipse::ContainsPoint(Point point) const {
    return (firstFocus.Dist(point) + secondFocus.Dist(point) <= width);
}

void Ellipse::Rotate(Point center, double angle) {
    firstFocus.Rotate(center, angle);
    secondFocus.Rotate(center, angle);
}

bool Ellipse::IsCongruentTo(const Shape& another) const {
    try {
        const Ellipse& other = dynamic_cast<const Ellipse&>(another);
        double dist = firstFocus.Dist(secondFocus);
        double otherDist = other.firstFocus.Dist(other.secondFocus);
        return !(std::abs(dist - otherDist) >= EPS || std::abs(width - other.width) >= EPS);
    } catch (const std::bad_cast& exception) {
        return false;
    }
}

void Ellipse::Reflex(Line axis) {
    firstFocus.Reflex(axis);
    secondFocus.Reflex(axis);
}

void Ellipse::Scale(Point center, double coefficient) {
    firstFocus.Scale(center, coefficient);
    secondFocus.Scale(center, coefficient);
    width *= coefficient;
}

bool Ellipse::operator==(const Shape& another) const {
    try {
        const Ellipse& other = dynamic_cast<const Ellipse&>(another);
        return ((firstFocus == other.firstFocus && secondFocus == other.secondFocus) ||
                (firstFocus == other.secondFocus && secondFocus == other.firstFocus)) &&
                (std::abs(width - other.width) >= EPS);
    } catch (const std::bad_cast& exception) {
        return false;
    }
}

double Ellipse::Perimeter() const {
    double a = width / 2;
    double b = a * sqrt(1 - Eccentricity() * Eccentricity());
    return PI * (3 * (a + b) - sqrt((3 * a + b) * (a + 3 * b)));
}

void Ellipse::Reflex(Point center) {
    firstFocus.Reflex(center);
    secondFocus.Reflex(center);
}

bool Ellipse::IsSimilarTo(const Shape& another) const {
    try {
        const Ellipse& other = dynamic_cast<const Ellipse&>(another);
        double dist = firstFocus.Dist(secondFocus);
        double otherDist = other.firstFocus.Dist(other.secondFocus);
        double ratio = dist / width;
        double otherRatio = otherDist / other.width;
        return (std::abs(ratio - otherRatio) < EPS);
    } catch (std::bad_cast exception) {
        return false;
    }
}

double Ellipse::Area() const {
    double a = width / 2;
    double b = a * sqrt(1 - Eccentricity() * Eccentricity());
    return PI * a * b;
}

//----------------------------------------------------------------------------------------------------------------------

class Circle: public Ellipse {
public:
    Circle(const Point& c, double r): Ellipse(c, c, 2 * r) {}

    double Radius() const;
};

double Circle::Radius() const {
    return width / 2;
}

//----------------------------------------------------------------------------------------------------------------------

class Rectangle: public Polygon {
public:
    Rectangle(const Point& first, const Point& second, double ratio);

    Point Center() const;
    std::pair<Line, Line> Diagonals() const;
};

Rectangle::Rectangle(const Point& first, const Point& second, double ratio): Polygon(std::vector<Point>()) {
    if (ratio < 1) {
        ratio = 1 / ratio;
    }
    double cos = sqrt(1 / (1 + ratio * ratio));
    double sin = sqrt(1 - cos * cos);
    Vector smallSide = (second - first).Rotate(sin, cos);
    smallSide = smallSide * cos;
    Vector bigSide = (second - first).Rotate(-cos, sin);
    bigSide = bigSide * sin;
    points = {first, first + smallSide, second, first + bigSide};
}

Point Rectangle::Center() const {
    return GetMiddle(points[0], points[2]);
}

std::pair<Line, Line> Rectangle::Diagonals() const {
    return {Line(points[0], points[2]), Line(points[1], points[3])};
}

//----------------------------------------------------------------------------------------------------------------------

class Square: public Rectangle {
public:
    Square(const Point& first, const Point& second): Rectangle(first, second, 1) {}

    Circle CircumscribedCircle() const;
    Circle InscribedCircle() const;
};

Circle Square::CircumscribedCircle() const {
    return Circle(Center(), (points[0] - points[2]).Abs() / 2);
}

Circle Square::InscribedCircle() const {
    return Circle(Center(), (points[0] - points[1]).Abs() / 2);
}

//----------------------------------------------------------------------------------------------------------------------

class Triangle: public Polygon {
public:
    Triangle(const Point& first, const Point& second, const Point& third): Polygon(std::vector<Point>({first, second, third})) {}

    Circle CircumscribedCircle() const;
    Point CircumscribedCircleCenter() const;
    Circle InscribedCircle() const;
    Point Centroid() const;
    Point Orthocenter() const;
    Line EulerLine() const;
    Circle NinePointsCircle() const;
};

Circle Triangle::CircumscribedCircle() const {
    Point center = CircumscribedCircleCenter();
    double radius = center.Dist(points[0]);
    return Circle(center, radius);
}

Point Triangle::CircumscribedCircleCenter() const {
    Line midPerpendicular1 = GetMidPerpendicular(points[0], points[1]);
    Line midPerpendicular2 = GetMidPerpendicular(points[0], points[2]);
    Point center = midPerpendicular1.Intersection(midPerpendicular2);
    return center;
}

Point Triangle::Centroid() const {
    double midX = 0;
    double midY = 0;
    for (const Point& point: points) {
        midX += point.x;
        midY += point.y;
    }
    midX /= 3;
    midY /= 3;
    return Point(midX, midY);
}

Circle Triangle::InscribedCircle() const {
    Line bisector1 = GetBisector(points[0], points[1], points[2]);
    Line bisector2 = GetBisector(points[1], points[2], points[0]);
    Point center = bisector1.Intersection(bisector2);
    double radius = Line(points[0], points[1]).Dist(center);
    return Circle(center, radius);
}

Point Triangle::Orthocenter() const {
    Line perpendicular1 = Line(points[0], points[0] + (points[1] - points[2]).Orthogonal());
    Line perpendicular2 = Line(points[1], points[1] + (points[2] - points[0]).Orthogonal());
    return perpendicular1.Intersection(perpendicular2);
}

Line Triangle::EulerLine() const {
    return Line(Orthocenter(), CircumscribedCircleCenter());
}

Circle Triangle::NinePointsCircle() const {
    Point center = GetMiddle(Orthocenter(), CircumscribedCircleCenter());
    double radius = center.Dist(GetMiddle(points[0], points[1]));
    return Circle(center, radius);
}
