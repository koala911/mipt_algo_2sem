#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "SegmentTree.h"

TEST_CASE("Test one") {
    INFO("TEST_CASE:Test one started!");
    std::vector<int> sequence = {1, 2, 3};
    SegmentTree segmentTree(sequence);
    REQUIRE(segmentTree.GetSegmentMax(0, 1), 2);
}


TEST_CASE("Test two") {
    INFO("TEST_CASE:Test two started!");
    std::vector<int> sequence = {1, 2, 3};
    SegmentTree segmentTree(sequence);
    segmentTree.IncreaseSegment(0, 1, 2)
    REQUIRE(segmentTree.GetSegmentMax(0, 1), 3);
}

TEST_CASE("Test three") {
    INFO("TEST_CASE:Test three started!");
    std::vector<int> sequence = {33, 22, 11};
    SegmentTree segmentTree(sequence);
    REQUIRE(segmentTree.GetSegmentMax(0, 1), 33);
}