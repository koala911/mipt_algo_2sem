#include "SegmentTree.h"


SegmentTree::SegmentTree(const std::vector<int>& sequence): size(size_t(1) << size_t(ceil(log2(sequence.size())))) {
    treeArray.assign(2 * size - 1, std::pair<int, int>(0, 0));
    for (size_t i = 0; i < sequence.size(); i++) {
        treeArray[treeArray.size() / 2 + i].first = sequence[i];
    }
    for (long i = treeArray.size() / 2 - 1; i >= 0; i--) {
        treeArray[i].first = std::max(treeArray[2 * i + 1].first, treeArray[2 * i + 2].first);
    }
}

void SegmentTree::IncreaseSegment(size_t a, size_t b, int add) {
    increaseSegmentInNode(0, 0, size - 1, a, b, add);
}

int SegmentTree::GetSegmentMax(size_t a, size_t b) {
    return getSegmentMaxFromNode(0, 0, size - 1, a, b);
}

int SegmentTree::getSegmentMaxFromNode(size_t nodeIndex, size_t left, size_t right, size_t a, size_t b) {
    if (a > b || left > right) {
        return 0;
    }
    if (a == left && b == right) {
        return treeArray[nodeIndex].first + treeArray[nodeIndex].second;
    }
    size_t med = (left + right) / 2;
    int leftMax = getSegmentMaxFromNode(2 * nodeIndex + 1, left, med, a, std::min(b, med));
    int rightMax = getSegmentMaxFromNode(2 * nodeIndex + 2, med + 1, right, std::max(a, med + 1), b);
    return std::max(rightMax, leftMax) + treeArray[nodeIndex].second;
}

void SegmentTree::increaseSegmentInNode(size_t nodeIndex, size_t left, size_t right, size_t a, size_t b, int add) {
    if (a > b || left > right) {
        return;
    }
    if (a == left && b == right) {
        treeArray[nodeIndex].second += add;
        return;
    }
    size_t med = (left + right) / 2;
    increaseSegmentInNode(2 * nodeIndex + 1, left, med, a, std::min(b, med), add);
    increaseSegmentInNode(2 * nodeIndex + 2, med + 1, right, std::max(a, med + 1), b, add);
    treeArray[nodeIndex].first = std::max(treeArray[2 * nodeIndex + 1].first + treeArray[2 * nodeIndex + 1].second,
                                          treeArray[2 * nodeIndex + 2].first + treeArray[2 * nodeIndex + 2].second);
}

