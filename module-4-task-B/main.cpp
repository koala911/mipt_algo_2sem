#include <iostream>
#include "SegmentTree.h"

int main() {
    size_t size;
    std::cin >> size;
    std::vector<int> tickets(size - 1);
    for (int i = 0; i < size - 1; i++) {
        std::cin >> tickets[i];
    }
    SegmentTree segmentTree(tickets);
    int capacity;
    std::cin >> capacity;
    size_t queryNumber;
    std::cin >> queryNumber;
    for (size_t i = 0; i < queryNumber; i++) {
        size_t left = 0;
        size_t right = 0;
        int add = 0;
        std::cin >> left >> right >> add;
        if (segmentTree.GetSegmentMax(left, right - 1) + add > capacity) {
            std::cout << i << ' ';
        } else {
            segmentTree.IncreaseSegment(left, right - 1, add);
        }
    }
    return 0;
}
