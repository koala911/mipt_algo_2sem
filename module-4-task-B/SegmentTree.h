#include <cmath>
#include <vector>


class SegmentTree{
public:
    explicit SegmentTree(const std::vector<int>& sequence);

    int GetSegmentMax(size_t a, size_t b);
    void IncreaseSegment(size_t a, size_t b, int add);

private:
    std::vector<std::pair<int, int>> treeArray;
    const size_t size;

    int getSegmentMaxFromNode(size_t nodeIndex, size_t left, size_t right, size_t a, size_t b);
    void increaseSegmentInNode(size_t nodeIndex, size_t left, size_t right, size_t a, size_t b, int add);
};