#include <iostream>
#include <vector>
#include <queue>
#include <stack>

enum TVerticesColorType {
    VCT_WHITE,
    VCT_GREY,
    VCT_BLACK   
};

bool DFS(const std::vector<std::vector<int>>& edges, int u, std::vector<int>& verticesColor, std::vector<int>& vertices) {
    verticesColor[u] = VCT_GREY;
    for(auto t: edges[u]){
        if(verticesColor[t] == VCT_GREY) {
            return false;
        }
        if(verticesColor[t] == VCT_WHITE) {
            if(!DFS(edges, t, verticesColor, vertices)) {
                return false;
            }
        }
    }
    verticesColor[u] = VCT_BLACK;
    vertices.push_back(u);
    return true;
}

void Solve(int numVertices, const std::vector<std::vector<int>>& edges) {
    std::vector<int> vertices;
    std::vector<int> verticesColor(numVertices);
    for(auto& c: verticesColor) {
        c = VCT_WHITE;
    }
    for(int u = 0; u < numVertices; ++u) {
        if(verticesColor[u] == VCT_WHITE ) {
            if(!DFS(edges, u, verticesColor, vertices)) {
                std::cout << "NO";
                return;
            }
        }
    }
    std::cout << "YES" << std::endl;
    for(int i = numVertices - 1; i >= 0; --i) {
        std::cout << vertices[i] << " ";
    }
}

int main() {
    int numVertices = 0; 
    int numEdges = 0;
    int u = 0;
    int v = 0;
    std::cin >> numVertices >> numEdges;
    std::vector<std::vector<int>> edges(numVertices);
    for(int i = 0; i < numEdges; ++i) {
        std::cin >> v >> u;
        edges[v].push_back(u);
    }
    Solve(numVertices, edges);
}