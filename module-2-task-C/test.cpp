#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "GetMinProbability.h"
#include <cmath>
#include <vector>

const double EPS = 1e-10;

TEST_CASE("Test from task") {
    INFO("TEST_CASE:Test from task started!");
    int numOfVertices = 5;
    int start = 1;
    int end = 3;
    std::vector<std::vector<bool>> adjacencyMatrix = {
            {0, 1, 1, 1, 1},
            {1, 0, 1, 0, 0},
            {1, 1, 0, 1, 0},
            {1, 0, 1, 0, 1},
            {1, 0, 0, 1, 0}
    };
    std::vector<std::vector<double>> probabilityMatrix = {
            {0, 0.2, 0.5, 0.67, 0.37},
            {0.2, 0, 0.2, 0, 0},
            {0.5, 0.2, 0, 0.1, 0},
            {0.67, 0, 0.1, 0, 0.92},
            {0.37, 0, 0, 0.92, 0}
    };
    double result = getMinProbability(numOfVertices, adjacencyMatrix, probabilityMatrix, start - 1, end - 1);
    REQUIRE(std::abs(0.36 - result) < EPS);
}

TEST_CASE("Test zero probability") {
    INFO("TEST_CASE:TTest zero probability started!");
    int numOfVertices = 5;
    int start = 1;
    int end = 3;
    std::vector<std::vector<bool>> adjacencyMatrix = {
            {0, 1, 1, 1, 1},
            {1, 0, 1, 0, 0},
            {1, 1, 0, 1, 0},
            {1, 0, 1, 0, 1},
            {1, 0, 0, 1, 0}
    };
    std::vector<std::vector<double>> probabilityMatrix = {
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
    };
    double result = getMinProbability(numOfVertices, adjacencyMatrix, probabilityMatrix, start - 1, end - 1);
    REQUIRE(std::abs(0 - result) < EPS);
}

TEST_CASE("Test unit probability") {
    INFO("TEST_CASE:TTest unit probability started!");
    int numOfVertices = 5;
    int start = 1;
    int end = 3;
    std::vector<std::vector<bool>> adjacencyMatrix = {
            {0, 1, 1, 1, 1},
            {1, 0, 1, 0, 0},
            {1, 1, 0, 1, 0},
            {1, 0, 1, 0, 1},
            {1, 0, 0, 1, 0}
    };
    std::vector<std::vector<double>> probabilityMatrix = {
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1}
    };
    double result = getMinProbability(numOfVertices, adjacencyMatrix, probabilityMatrix, start - 1, end - 1);
    REQUIRE(std::abs(1 - result) < EPS);
}
