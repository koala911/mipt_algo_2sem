#pragma once

#include <vector>

double getMinProbability(int numOfVertices, const std::vector<std::vector<bool>>& adjacencyMatrix,
                         const std::vector<std::vector<double>>& probabilityMatrix, int start, int end);