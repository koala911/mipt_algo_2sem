#include "GetMinProbability.h"
#include <vector>

// Используется алгоритм Флойда
double getMinProbability(int numOfVertices, const std::vector<std::vector<bool>>& adjacencyMatrix,
                         const std::vector<std::vector<double>>& probabilityMatrix, int start, int end) {
    std::vector<std::vector<double>> maxProbabilityOfComplement = probabilityMatrix;
    std::vector<std::vector<bool>> reachabilityMatrix = adjacencyMatrix;
    for (int i = 0; i < numOfVertices; ++i) {
        for (int j = 0; j < numOfVertices; ++j) {
            if (adjacencyMatrix[i][j]) {
                maxProbabilityOfComplement[i][j] = 1 - maxProbabilityOfComplement[i][j];
            }
        }
    }
    for (int k = 0; k < numOfVertices; ++k) {
        for (int i = 0; i < numOfVertices; ++i) {
            for (int j = 0; j < numOfVertices; ++j) {
                if (reachabilityMatrix[i][k] && reachabilityMatrix[k][j]) {
                    reachabilityMatrix[i][j] = true;
                    double probabilityFromIToJAcrossK = maxProbabilityOfComplement[i][k] * maxProbabilityOfComplement[k][j];
                    maxProbabilityOfComplement[i][j] = std::max(maxProbabilityOfComplement[i][j], probabilityFromIToJAcrossK);
                }
            }
        }
    }
    return 1 - maxProbabilityOfComplement[start][end];
}
