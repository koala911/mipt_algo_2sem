#include "GetMinProbability.h"
#include <iostream>
#include <vector>

int main() {
    int numOfVertices;
    int numOfEdges;
    int start;
    int end;
    std::cin >> numOfVertices >> numOfEdges >> start >> end;
    int u;
    int v;
    int p;
    std::vector<std::vector<bool>> adjacencyMatrix(numOfVertices, std::vector<bool>(numOfVertices, false));
    std::vector<std::vector<double>> probabilityMatrix(numOfVertices, std::vector<double>(numOfVertices, 0));
    for (int i = 0; i < numOfEdges; ++i) {
        std::cin >> u >> v >> p;
        adjacencyMatrix[u - 1][v - 1] = true;
        adjacencyMatrix[v - 1][u - 1] = true;
        probabilityMatrix[u - 1][v - 1] = double(p) / 100;
        probabilityMatrix[v - 1][u - 1] = double(p) / 100;
    }
    std::cout << getMinProbability(numOfVertices, adjacencyMatrix, probabilityMatrix, start - 1, end - 1);
}
