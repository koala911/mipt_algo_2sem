#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "MinCut.h"

TEST_CASE("Test example") {
    INFO("TEST_CASE:Test example started!");
    int numOfVertices = 4;
    std::vector<std::vector<int>> adjacencyMatrix = {
            {0, 1, 1, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 1, 1, 0}
    };
    std::vector<int> minCut;
    MinCut(numOfVertices, adjacencyMatrix, minCut);
    int cutSize = 0;
    std::vector<bool> inMinCut(numOfVertices, false);
    for (auto v: minCut) {
        inMinCut[v] = true;
    }
    for (int v = 0; v < numOfVertices; ++v) {
        for (int u = 0; u < numOfVertices; ++u) {
            if (inMinCut[v] && !inMinCut[u] && adjacencyMatrix[u][v]) {
                ++cutSize;
            }
        }
    }
    REQUIRE(cutSize, 2);
}

TEST_CASE("Test zero matrix") {
    INFO("TEST_CASE:Test zero matrix started!");
    int numOfVertices = 4;
    std::vector<std::vector<int>> adjacencyMatrix = {
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    };
    std::vector<int> minCut;
    MinCut(numOfVertices, adjacencyMatrix, minCut);
    int cutSize = 0;
    std::vector<bool> inMinCut(numOfVertices, false);
    for (auto v: minCut) {
        inMinCut[v] = true;
    }
    for (int v = 0; v < numOfVertices; ++v) {
        for (int u = 0; u < numOfVertices; ++u) {
            if (inMinCut[v] && !inMinCut[u] && adjacencyMatrix[u][v]) {
                ++cutSize;
            }
        }
    }
    REQUIRE(cutSize, 0);
}

TEST_CASE("Test unit matrix") {
    INFO("TEST_CASE:Test unit matrix started!");
    int numOfVertices = 4;
    std::vector<std::vector<int>> adjacencyMatrix = {
        {1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, 1, 1, 1}
    };
    std::vector<int> minCut;
    MinCut(numOfVertices, adjacencyMatrix, minCut);
    int cutSize = 0;
    std::vector<bool> inMinCut(numOfVertices, false);
    for (auto v: minCut) {
        inMinCut[v] = true;
    }
    for (int v = 0; v < numOfVertices; ++v) {
        for (int u = 0; u < numOfVertices; ++u) {
            if (inMinCut[v] && !inMinCut[u] && adjacencyMatrix[u][v]) {
                ++cutSize;
            }
        }
    }
    REQUIRE(cutSize, 3);
}