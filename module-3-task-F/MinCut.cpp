#include "MinCut.h"

void MinCut(int numOfVertices, std::vector<std::vector<int>>& adjacencyMatrix, std::vector<int>& minCut) {
    std::vector<std::vector<int>> vertices(numOfVertices);
    for (int i = 0; i < numOfVertices; ++i) {
        vertices[i].assign(1, i);
    }
    std::vector<int> weight(numOfVertices);
    std::vector<bool> exist(numOfVertices, true);
    std::vector<int> inFirstPart(numOfVertices);
    int bestCost = -1;
    for (int iteration = 0; iteration < numOfVertices - 1; ++iteration) {
        inFirstPart.assign(inFirstPart.size(), false);
        weight.assign(weight.size(), 0);
        for (int it = 0, prev = 0; it < numOfVertices - iteration; ++it) {
            int current = -1;
            for (int i = 0; i < numOfVertices; ++i) {
                if (exist[i] && !inFirstPart[i] && (current == -1 || weight[i] > weight[current])) {
                    current = i;
                }
            }
            if (it == numOfVertices - iteration - 1) {
                if (weight[current] < bestCost || bestCost == -1){
                    bestCost = weight[current];
                    minCut = vertices[current];
                }
                vertices[prev].insert( vertices[prev].end(), vertices[current].begin(), vertices[current].end());
                for (int i = 0; i < numOfVertices; ++i) {
                    adjacencyMatrix[prev][i] = adjacencyMatrix[i][prev] += adjacencyMatrix[current][i];
                }
                exist[current] = false;
            } else {
                inFirstPart[current] = true;
                for (int i = 0; i < numOfVertices; ++i) {
                    weight[i] += adjacencyMatrix[current][i];
                }
                prev = current;
            }
        }
    }
}
