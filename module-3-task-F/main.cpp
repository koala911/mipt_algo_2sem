#include <iostream>
#include <vector>
#include <unordered_set>
#include "MinCut.h"

int main() {
    int numOfVertices;
    std::cin >> numOfVertices;
    std::vector<std::vector<int>> adjacencyMatrix(numOfVertices, std::vector<int>(numOfVertices, 0));
    for (int i = 0; i < numOfVertices; ++i) {
        for (int j = 0; j < numOfVertices; ++j) {
            char symbol;
            std::cin >> symbol;
            if (symbol - '0') {
                adjacencyMatrix[i][j] = 1;
            }
        }
    }
    std::vector<int> minCut;
    MinCut(numOfVertices, adjacencyMatrix, minCut);
    std::unordered_set<int> used;
    for (auto v: minCut) {
        std::cout << v + 1 << " ";
        used.insert(v);
    }
    std::cout << std::endl;
    for (int v = 0; v < numOfVertices; ++v) {
        if (!used.count(v)) {
            std::cout << v + 1 << " ";
        }
    }
}
