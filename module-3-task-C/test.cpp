#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "Graph.h"

TEST_CASE("Test example 1") {
INFO("TEST_CASE:Test example 1 started!");
    int numOfVertices = 3;
    int numOfBonusEdges = 2;
    Graph graph(numOfVertices);
    graph.Add(1, 0);
    graph.Add(3, 1);
    graph.Add(3, 2);
    graph.Add(1, 2, 5);
    graph.Add(1, 0, 1);
    REQUIRE(graph.FindMinCost(), 5);
}

TEST_CASE("Test example 2") {
    INFO("TEST_CASE:Test example 2 started!");
    int numOfVertices = 4;
    int numOfBonusEdges = 0;
    Graph graph(numOfVertices);
    graph.Add(1, 0);
    graph.Add(3, 1);
    graph.Add(3, 2);
    graph.Add(7, 3);
    REQUIRE(graph.FindMinCost(), 16);
}


TEST_CASE("Test example 2") {
INFO("TEST_CASE:Test example 2 started!");
    int numOfVertices = 5;
    int numOfBonusEdges = 4;
    Graph graph(numOfVertices);
    graph.Add(1, 0);
    graph.Add(2, 1);
    graph.Add(3, 2);
    graph.Add(4, 3);
    graph.Add(5, 4);
    graph.Add(0, 1, 8);
    graph.Add(0, 2, 10);
    graph.Add(0, 3, 7);
    graph.Add(0, 4, 15);
    REQUIRE(graph.FindMinCost(), 18);
}