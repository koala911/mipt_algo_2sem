#include "Graph.h"

class DSU {
private:
    std::vector<int> parents;
public:
    explicit DSU(int size) {
        parents.resize(size);
        for (int i = 0; i < size; ++i) {
            parents[i] = i;
        }
    }

    int Find(int element) {
        if (element == parents[element]) {
            return element;
        }
        else {
            return(parents[element] = Find(parents[element]));
        }
    }

    void Union(int element1, int element2) {
        element1 = Find(element1);
        element2 = Find(element2);
        parents[element1] = element2;
    }
};

struct Edge {
    int to;
    int from;
    long long cost;

    explicit Edge(int from = 0, int to = 0, long long cost = -1): from(from), to(to), cost(cost) {}
};

bool EdgeComparator(const Edge& left, const Edge& right) {
    return left.cost < right.cost;
}

Graph::Graph(int size) {
    numOfVertices = size;
    weights.resize(size);
    weights[0] = 0;
};

void Graph::Add(int from, int to, long long cost) {
    adjacencyList.emplace_back(from, to, cost);
}

void Graph::Add(long long weight, int vertex) {
    weights[vertex] = weight;
    if (weight < minWeight) {
        minWeight = weight;
        indexOfMinWeight = vertex;
    }
}

long long Graph::FindMinCost() {
    for (int i = 0; i < numOfVertices; ++i) {
        if (i != indexOfMinWeight) {
            Add(i, indexOfMinWeight, weights[i] + minWeight);
        }
    }
    return KruskalAlgorithm();
}

long long Graph::KruskalAlgorithm() {
    long long totalCost = 0;
    DSU trees(numOfVertices);
    sort(adjacencyList.begin(), adjacencyList.end(), EdgeComparator);
    for (const auto& edge: adjacencyList) {
        if (trees.Find(edge.from) != trees.Find(edge.to)) {
            totalCost += edge.cost;
            trees.Union(edge.from, edge.to);
        }
    }
    return totalCost;
}
