#include <algorithm>
#include <iostream>
#include <vector>
#include "Graph.h"

int main() {
    int numOfVertices;
    int numOfBonusEdges;
    std::cin >> numOfVertices >> numOfBonusEdges;
    Graph graph(numOfVertices);
    for (int i = 0; i < numOfVertices; ++i) {
        long long weight;
        std::cin >> weight;
        graph.Add(weight, i);
    }
    for (int i = 0; i < numOfBonusEdges; ++i) {
        int from;
        int to;
        long long cost;
        std::cin >> from >> to >> cost;
        graph.Add(from - 1, to - 1, cost);
    }
    std::cout << graph.FindMinCost();
}