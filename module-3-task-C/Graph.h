#include <vector>

const long long INF = 10000000000000;

struct Edge;

class Graph {
public:
    explicit Graph(int size);

    void Add(int from, int to, long long cost);

    void Add(long long weight, int vertex);

    long long FindMinCost();

private:
    std::vector<Edge> adjacencyList;
    int numOfVertices;
    std::vector<long long> weights;
    long long minWeight = INF;
    int indexOfMinWeight = 0;

    long long KruskalAlgorithm();
};