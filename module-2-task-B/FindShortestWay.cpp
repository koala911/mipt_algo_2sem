#include "FindShortestWay.h"

const int INF = 400000000;

// Используется алгоритм Форда-Беллмана
int FindShortestWay(int numOfVertices, std::vector<std::vector<int>>& weights, int start, int end, int maxDist) {
    std::vector<std::vector<int>> shortestWays(numOfVertices, std::vector<int>(maxDist + 1 ,INF));
    // shortestWays[u][i] - кратчайший путь от start до u за i шагов
    shortestWays[start][0] = 0;
    for (int i = 1; i < maxDist + 1; ++i) {
        for (int u = 0; u < numOfVertices; ++u) {
            for (int v = 0; v < numOfVertices; ++v) {
                if (weights[u][v] > 0 && shortestWays[v][i] > shortestWays[u][i - 1] + weights[u][v]) {
                    shortestWays[v][i] = shortestWays[u][i - 1] + weights[u][v];
                }
            }
        }
    }
    int answer = INF;
    for (int i = 0; i < maxDist + 1; ++i) {
        if (answer > shortestWays[end][i]) {
            answer = shortestWays[end][i];
        }
    }
    if (answer == INF) {
        return -1;
    }
    return answer;
}