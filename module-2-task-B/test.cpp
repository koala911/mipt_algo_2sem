#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "FindShortestWay.h"

TEST_CASE("Test example 1") {
    INFO("TEST_CASE:Test example 1 started!");
    int numOfVertices = 5;
    int k = 2;
    int start = 4;
    int end = 1;
    std::vector<std::vector<int>> weights = {
            {0, 6, 0, 0, 0},
            {0, 0, 0, 0, 7},
            {0, 0, 0, 0, 1},
            {9, 0, 2, 0, 3},
            {1, 0, 0, 0, 0}
    };
    int result = FindShortestWay(numOfVertices, weights, start - 1, end - 1, k);
    REQUIRE(result == 4);
}

TEST_CASE("Test example 2") {
    INFO("TEST_CASE:Test example 2 started!");
    int numOfVertices = 3;
    int k = 1;
    int start = 1;
    int end = 3;
    std::vector<std::vector<int>> weights = {
            {0, 4, 0},
            {0, 0, 5},
            {6, 0, 0},
    };
    int result = FindShortestWay(numOfVertices, weights, start - 1, end - 1, k);
    REQUIRE(result == -1);
}

TEST_CASE("Test complete graph") {
    INFO("TEST_CASE:Test complete graph started!");
    int numOfVertices = 5;
    int k = 2;
    int start = 1;
    int end = 5;
    std::vector<std::vector<int>> weights = {
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1}
    };
    int result = FindShortestWay(numOfVertices, weights, start - 1, end - 1, k);
    REQUIRE(result == 1);
}