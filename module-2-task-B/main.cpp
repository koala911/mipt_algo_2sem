#include <iostream>
#include <vector>
#include "FindShortestWay.h"

int main() {
    int numOfVertices;
    int numOfEdges;
    int start;
    int end;
    int k;
    std::cin >> numOfVertices >> numOfEdges >> k >> start >> end;
    std::vector<std::vector<int>> weights(numOfVertices, std::vector<int>(numOfVertices, 0));
    int u;
    int v;
    int weight;
    for (int i = 0; i < numOfEdges; ++i) {
        std::cin >> u >> v >> weight;
        if (weights[u - 1][v - 1] == 0 || weights[u - 1][v - 1] > weight) {
            weights[u - 1][v - 1] = weight;
        }
    }
    std::cout << findShortestWay(numOfVertices, weights, start - 1, end - 1, k);
}
