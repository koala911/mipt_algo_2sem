#pragma once

#include <vector>

int FindShortestWay(int numOfVertices, std::vector<std::vector<int>>& weights, int start, int end, int maxDist);
