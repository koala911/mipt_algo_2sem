#include <iostream>
#include <queue>
#include <vector>

enum TEntityNameType {
    NT_LEON,
    NT_MATILDA,
    NT_MILK
};

void BFS(const std::vector<std::vector<int>>& edges, int vertex, std::vector<int>& distance) {
    distance[vertex] = 0;
    std::queue<int> verticesQueue;
    verticesQueue.push(vertex);
    while(!verticesQueue.empty()) {
        int u = verticesQueue.front();
        verticesQueue.pop();
        for(const auto& t: edges[u]) {
            if(distance[t] == -1) {
                verticesQueue.push(t);
                distance[t] = distance[u] + 1;
            }
        }
    }
}

//ищет минимальное кол-во ребер, по которым придется пройти Леону и Матильде
int MinDistToMilk(int numVertices, int numEdges, const std::vector<int>& entityPosition, const std::vector<std::vector<int>>& edges) {
    std::vector<std::vector<int>> entityDistance(3);
    for(int entity: {NT_MILK, NT_LEON, NT_MATILDA}) {
        entityDistance[entity].resize(numVertices);
        for(int i = 0; i < numVertices; ++i) {
            entityDistance[entity][i] = -1;
        }
    }
    for(int entity: {NT_MILK, NT_LEON, NT_MATILDA}) {
        BFS(edges, entityPosition[entity], entityDistance[entity]);
    }
    int minDistance = 2 * numEdges + 1;
    for(int i = 0; i < numEdges; ++i) {
        int totalDistance = entityDistance[NT_MATILDA][i] + entityDistance[NT_LEON][i] + entityDistance[NT_MILK][i];
        if(minDistance > totalDistance) {
            minDistance = totalDistance;
        }
    }
    return minDistance;
}

int main() {
    int numVertices = 0;
    int numEdges = 0;
    std::vector<int> entityPosition(3);
    int v = 0;
    int u = 0;
    std::cin >> numVertices >> numEdges;
    for(int entity: {NT_MILK, NT_LEON, NT_MATILDA}) {
        std::cin >> entityPosition[entity];
        --entityPosition[entity];
    }
    std::vector<std::vector<int>> edges(numVertices);
    for(int i = 0; i < numEdges; ++i) {
        std::cin >> v >> u;
        edges[v - 1].push_back(u - 1);
        edges[u - 1].push_back(v - 1);
    }
    std::cout << MinDistToMilk(numVertices, numEdges, entityPosition, edges);
}