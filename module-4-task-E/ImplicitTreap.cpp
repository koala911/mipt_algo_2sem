#include "ImplicitTreap.h"


ImplicitTreap::ImplicitTreap(): root(nullptr) {}

ImplicitTreap::~ImplicitTreap() {
    clear(root);
}

void ImplicitTreap::clear(Node* node) {
    if (node == nullptr) {
        return;
    }
    clear(node->left);
    clear(node->right);
    delete node;
}

void ImplicitTreap::InsertAt(int position, const std::string& value) {
    Node* current = new Node(value);
    std::pair<Node*, Node*> splitted = split(root, position);
    Node* right = merge(current, splitted.second);
    root = merge(splitted.first, right);
}

void ImplicitTreap::DeleteAt(int position) {
    std::pair<Node*, Node*> splitted = split(root, position);
    std::pair<Node*, Node*> splittedChild = split(splitted.second, 1);
    delete splittedChild.first;
    root = merge(splitted.first, splittedChild.second);
}

std::string ImplicitTreap::GetAt(int position) const {
    int current;
    Node* node = root;
    while (true) {
        current = (node->left != nullptr)?(node->left->size):0;
        if (current == position) {
            break;
        }
        if (position < current) {
            node = node->left;
        }
        else {
            position -= current + 1;
            node = node->right;
        }
    }
    return node->value;
}

std::pair<ImplicitTreap::Node*, ImplicitTreap::Node*> ImplicitTreap::split(Node* node, int position) {
    if (node == nullptr) {
        return std::pair<Node*, Node*>(nullptr, nullptr);
    }
    int current = (node->left != nullptr)?(node->left->size) : 0;
    if (current >= position) {
        std::pair<Node*, Node*> ans = split(node->left, position);
        node->left = ans.second;
        node->FixSize();
        return std::make_pair(ans.first, node);
    }
    else {
        std::pair<Node*, Node*> ans = split(node->right, position - current - 1);
        node->right = ans.first;
        node->FixSize();
        return std::make_pair(node, ans.second);
    }
}

ImplicitTreap::Node* ImplicitTreap::merge(Node* left, Node* right) {
    if (right == nullptr) {
        return left;
    }
    if (left == nullptr) {
        return right;
    }
    if (left->priority > right->priority) {
        left->right = merge(left->right, right);
        left->FixSize();
        return left;
    }
    else {
        right->left = merge(left, right->left);
        right->FixSize();
        return right;
    }
}


ImplicitTreap::Node::Node(const std::string& value): value(value), priority(rand()), left(nullptr), right(nullptr), size(1) {}

void ImplicitTreap::Node::FixSize() {
    size = 1;
    if (left != nullptr) {
        size += left->size;
    }
    if (right != nullptr) {
        size += right->size;
    }
}
