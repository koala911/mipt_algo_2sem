#include <string>


class ImplicitTreap{
public:
    ImplicitTreap();
    ~ImplicitTreap();

    void InsertAt(int position, const std::string& value);
    void DeleteAt(int position);
    std::string GetAt(int position) const;

private:
    struct Node{
        explicit Node(const std::string& value);

        Node* left;
        Node* right;
        int size;
        int priority;
        std::string value;

        void FixSize();
    };

    Node* root;

    std::pair<Node*, Node*> split(Node* node, int position);
    Node* merge(Node* node1, Node* node2);
    void clear(Node* node);
};
