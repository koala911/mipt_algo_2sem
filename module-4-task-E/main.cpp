#include <iostream>
#include "ImplicitTreap.h"

int main() {
    int k;
    std::cin >> k;
    ImplicitTreap stringArray;
    for (int i = 0; i < k; ++i) {
        char command;
        std::cin >> command;
        if (command == '+') {
            int position;
            std::string str;
            std::cin >> position >> str;
            stringArray.InsertAt(position, str);
        } else if (command == '?') {
            int position;
            std::cin >> position;
            std::cout << stringArray.GetAt(position) << std::endl;
        } else if (command == '-') {
            int positionFrom;
            int positionTo;
            std::cin >> positionFrom >> positionTo;
            for (int j = 0; j <= positionTo - positionFrom; ++j) {
                stringArray.DeleteAt(positionFrom);
            }
        }
    }
}
