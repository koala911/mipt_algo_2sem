#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"
#include "ImplicitTreap.h"

TEST_CASE("Test one") {
    INFO("TEST_CASE:Test one started!");
    ImplicitTreap treap;
    treap.InsertAt(0, "abc");
    REQUIRE(treap.GetAt(0) == "abc");
}

TEST_CASE("Test two") {
    INFO("TEST_CASE:Test two started!");
    ImplicitTreap treap;
    treap.InsertAt(0, "abc");
    treap.InsertAt(0, "cba");
    treap.DeleteAt(0);
    REQUIRE(treap.GetAt(0) == "abc");
}


TEST_CASE("Test three") {
    INFO("TEST_CASE:Test three started!");
    ImplicitTreap treap;
    treap.InsertAt(0, "abc");
    treap.InsertAt(1, "cba");
    REQUIRE(treap.GetAt(1) == "cba");
}